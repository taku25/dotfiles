#!/bin/zsh
echo "autoupdate vim"

# if [ -n "$LUAENV_ROOT" ]; then
#     echo "use luaenv"
#     if [[ "$OSTYPE" == "darwin"* ]]; then
#         export vi_cv_dll_name_lua="$LUAENV_ROOT/versions/luajit-2.0.5/bin/libluajit-5.1.dylib"
#     fi
# fi

LUA_VERSION=$HOME/.anyenv/envs/luaenv/versions/luajit-2.0.5
if [ -n "$PYENV_ROOT" ]; then
    echo "use pyenv python"
    export vi_cv_path_python="$PYENV_ROOT/versions/2.7.17/bin/python2"
    export vi_cv_path_python3="$PYENV_ROOT/versions/3.9.0/bin/python3"
    if [[ "$OSTYPE" == "darwin"* ]]; then
       export vi_cv_dll_name_python="$PYENV_ROOT/versions/2.7.17/Python.framework/Versions/2.7/Python"
       export vi_cv_dll_name_python3="$PYENV_ROOT/versions/3.9.0/Python.framework/Versions/3.9/Python"
       export LDFLAGS="-L/usr/local/opt/gettext/lib"
       export CPPFLAGS="-I/usr/local/opt/gettext/include"
       LUA_VERSION=$HOME/.anyenv/envs/luaenv/versions/5.4.0
    elif [[ "$OSTYPE" == "linux-gnu" ]]; then
       export vi_cv_dll_name_python="$PYENV_ROOT/versions/2.7.17/lib/libpython2.7.so"
       export vi_cv_dll_name_python3="$PYENV_ROOT/versions/3.9.0/lib/libpython3.9.so"
       LUA_VERSION=$HOME/.anyenv/envs/luaenv/versions/5.4.0
    fi
fi


git pull
cd src
rm auto/config.cache
make distclean

CC=clang ./configure --with-features=huge \
    --enable-multibyte \
    --enable-cscope \
    --with-compiledby=taku25 \
    --enable-fail-if-missing \
    --enable-perlinterp=dynamic \
    --enable-pythoninterp=dynamic \
    --enable-python3interp=dynamic \
    --enable-luainterp \
    --enable-rubyinterp=dynamic \
    --enable-gui=no \
    --with-tlib=ncurses  \
    --with-lua-prefix=$LUA_VERSION

    #--with-luajit \
if [ $? -eq 0 ]; then
    make
    sudo make install
else
    echo "configure not scucess"
fi

