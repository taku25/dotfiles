@echo off

setlocal

git pull

set VIM_DIR=%cd%
echo %VIM_DIR%
cd %VIM_DIR%/src

set INCLUDE=
set LIBPATH

set VS_DIR=C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\VC\Auxiliary\Build
set VS_ARC=amd64

@call "%VS_DIR%\vcvarsall.bat" %VS_ARC%
 

set USERNAME=taku25
set USERDOMAIN=
 
set PYTHONE3_DIR=C:\Users\taku3\AppData\Local\Programs\Python\Python39
set PYTHON3_ENV=PYTHON3=%PYTHONE3_DIR% ^
                DYNAMIC_PYTHON3=yes ^
                DYNAMIC_PYTHON3_DLL=python39.dll ^
                PYTHON3_VER=39
 

nmake -f Make_mvc.mak clean
nmake -f Make_mvc.mak FEATURE=HUGE GUI=yes ^
        PYTHON3=C:\Users\taku3\AppData\Local\Programs\Python\Python39 ^
        DYNAMIC_PYTHON3=yes PYTHON3_VER=39 DYNAMIC_PYTHON3_DLL="python39.dll"
 

 if not %ERRORLEVEL% == 0 (
     echo make ERRORLEVEL
     exit /b 1
 )
cd %VIM_DIR%

set INSTALL_DIR=C:\Users\taku3\App\vim
rd /s  %INSTALL_DIR%
 
if not exist %INSTALL_DIR%\ (
     mkdir %INSTALL_DIR%
     mkdir %INSTALL_DIR%\GvimExt64
)
 
xcopy /s runtime\* %INSTALL_DIR%
copy src\*.exe %INSTALL_DIR%
copy src\tee\tee.exe %INSTALL_DIR%
copy src\xxd\xxd.exe %INSTALL_DIR%
copy src\GvimExt\gvimext.dll %INSTALL_DIR%\GvimExt64 


rem python.dll
copy %PYTHONE3_DIR%\python39.dll  %INSTALL_DIR%
 
rem iconv
set ICONV_DIR=C:\Users\taku3\scoop\apps\gettext\current
copy %ICONV_DIR%\bin\libintl-8.dll  %INSTALL_DIR%
copy %ICONV_DIR%\bin\libiconv-2.dll  %INSTALL_DIR%
copy %ICONV_DIR%\bin\libintl-8.dll  %INSTALL_DIR%\GvimExt64
copy %ICONV_DIR%\bin\libiconv-2.dll  %INSTALL_DIR%\GvimExt64

endlocal
