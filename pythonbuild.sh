

PYTHON_CONFIGURE_OPTS="--enable-shared" \
    LDSHARED="clang -bundle" \
    LDCXXSHARED="clang++ -bundle" \
    BLDSHARED="clang -bundle -lpython2.7" \
    pyenv install 2.7.12

PYTHON_CONFIGURE_OPTS="--enable-shared" \
    LDSHARED="clang -bundle"\
    LDCXXSHARED="clang++ -bundle" \
    BLDSHARED="clang -bundle -lpython3.6m" \
    pyenv install 3.6.0b3
