﻿
function! s:PluginInitFunction_None() " {{{
endfunction " }}}

function! s:PluginInitFunction_unite_vim() " {{{
"    nnoremap    [unite]   <Nop>
"    nmap    <C-f> [unite]
"
"    nnoremap <silent> [unite]o  :<C-u>Unite outline<CR>
"    nnoremap <silent> [unite]g  :<C-u>Unite grep<CR>
"    nnoremap <silent> <C-b> :<C-u>Unite buffer<CR>
"    nnoremap <silent> <C-p>  :<C-u>call <SID>unite_file_async()<CR>
"
"    set wildignore=*.o,*.obj,*.la,*.lo,*.so,*.pyc,*.pyo,*.jpg,*.png,*.gif,*.xlsx,*.tga,*.JPEG,*.meta,*.fbx,*.mp4,*.aep,*.mb
"                \,*.pptx,*.psd,*.dll,*.pdb,*.xlsm,*.avi,*.deploy,*.vsto,*.mat,*.prefab,*.controller,*.anim
"                \,exe
"
"    call unite#custom#source('file_rec/git', 'ignore_globs', split(&wildignore, ','))
"    call unite#custom#source('file_rec/async', 'ignore_globs', split(&wildignore, ','))
"
"    function! s:unite_file_async()
"        let l:current_path = expand('%:p:h')
"
"        if isdirectory(l:current_path.'/.git')
"            "execute 'Unite file_rec/git:--cached:--others:--exclude-standard'
"            execute 'Unite file_rec/git:--exclude-standard'
"        else
"            execute 'Unite file_rec/async'
"        endif
"    endfunction
"
"    let g:unite_update_time = 5
"    call unite#custom#profile('default', 'context', {
"        \   'start_insert' : 1,
"        \   'direction' : 'botright',
"        \   'split' : 1 
"        \ })
"
"
"    " For optimize.
"    let g:unite_source_file_mru_filename_format = ''
"
"    if has('clientserver')
"
"        let s:action = {
"                    \ 'is_selectable' : 1,
"                    \ 'description' : 'send edit filename',
"                    \ }
"        function! s:action.func(candidates)
"
"            let isExistServer = 0
"            let serverNameList = split(serverlist(), '\r\?\n')
"
"            for server in serverNameList
"                if server == 'EDITOR'
"                    let isExistServer = 1
"                    break
"                endif
"            endfor
"
"            if isExistServer == 1
"                for candidate in a:candidates
"                    call remote_send("EDITOR", ":Edit ".candidate['word']."<CR>")
"                endfor
"            endif
"        endfunction
"
"        call unite#custom#action('file', 'remoteopen', s:action)
"
"        unlet s:action
"    endif
"
"
"    let s:action = {
"                \ 'is_selectable' : 0,
"                \ 'description' : 'search makefile',
"                \ }
"    function! s:action.func(candidate)
"        let g:lastMakeFilePath = a:candidate['word']
"    endfunction
"
"    call unite#custom#action('file', 'searchmake', s:action)
"    unlet s:action
"
"    let s:source = { 
"                \ 'name': "searchmake"
"                \ }
"    function! s:source.gather_candidates(args, context)
"        let l:searchDir = "./"
"        if len(a:args) == 1
"            let l:searchDir = a:args['arg1'];
"        endif
"        
"        let l:filelist = glob(l:searchDir.'**/Makefile*',0,1)
"        return  map(l:filelist, '{
"                    \   "word": v:val,
"                    \   "kind": "file",
"                    \   "action__path": v:val
"                    \ }')
"    endfunction
"    call unite#define_source(s:source)
"    unlet s:source
"    call unite#custom#default_action('source/searchmake/*', 'searchmake')
"
"    let s:unite_ignore_patterns = join([
"                                       \ '\.git/',
"                                       \ '\.bundle/',
"                                       \ '\.DS_Store',
"                                       \ '\.meta',
"                                       \ 'cmake-build-debug/',
"                                       \ '\.idea',
"                                       \ '\.vscode',
"                                      \ 'build/',
"                                       \ 'ide/',
"                                       \ ], '\|')
"     call unite#custom_source('file,file_rec,file_rec/async,grep', 'ignore_pattern', s:unite_ignore_patterns)
"     unlet s:unite_ignore_patterns
endfunction " }}}


function! s:PluginInitFunction_vimfiler() " {{{
    let g:vimfiler_as_default_explorer = 0
    "let g:vimfiler_as_default_explorer = 0
    let g:vimfiler_safe_mode_by_default = 0

    let g:filer_toggle = 0
    function! s:ToggleVimFilerExplorerWidth()
        let l:toggle = g:filer_toggle == 0 ? ">" : "<"
        let g:filer_toggle = !g:filer_toggle
        "200くらいでじゅうぶんだろなぁ〜....てけと
        "たりなくなったらfilenameからちゃんとだす
        return "200\<C-W>".l:toggle
    endfunction


    augroup my-vimfiler-setting
        autocmd!
        autocmd FileType vimfiler nnoremap <buffer><script><expr> <C-H> <SID>ToggleVimFilerExplorerWidth()."\|<SID>(vimfiler_redraw_screen)"
    augroup END
endfunction " }}}

function! s:PluginInitFunction_asyncomplete_vim() " {{{
    function! s:check_back_space() abort
        let col = col('.') - 1
        return !col || getline('.')[col - 1]  =~ '\s'
    endfunction


    let g:asyncomplete_auto_popup = 1
    let g:asyncomplete_popup_delay = 20
    let g:asyncomplete_auto_completeopt = 1

    " inoremap <expr> <cr>  pumvisible() ? asyncomplete#close_popup() : "<cr>"
    " inoremap <expr> <cr>  pumvisible() ? asyncomplete#close_popup() : "\<cr>"
    " inoremap <expr> <c-[>  pumvisible() ? asyncomplete#close_popup() : "\<c-[>"

    if s:is_installed('ultisnips')
        let g:ulti_expand_or_jump_res = 0
        function! <SID>ExpandOrClosePopup()
            let snippet = UltiSnips#ExpandSnippetOrJump()
            if g:ulti_expand_or_jump_res > 0
                return snippet
            else
                let close_popup = asyncomplete#close_popup()
                return close_popup
            endif
        endfunction
        inoremap <silent><expr><CR> pumvisible() ? "<C-R>=<SID>ExpandOrClosePopup()<CR>" : "\<Cr>"
    else
        inoremap <expr> <cr>  pumvisible() ? asyncomplete#close_popup() : "\<cr>"
    endif

endfunction " }}}

function! s:PluginInitFunction_ultisnips() " {{{
    let g:UltiSnipsJumpForwardTrigger="<tab>"
    let g:UltiSnipsJumpBackwardTrigger="<c-b>"


    let g:UltiSnipsExpandTrigger = "<nop>"
endfunction " }}}

function! s:PluginInitFunction_asyncomplete_file() " {{{
    
        au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#file#get_source_options({
       \ 'name': 'file',
       \ 'allowlist': ['*'],
       \ 'priority': 10,
       \ 'completor': function('asyncomplete#sources#file#completor')
       \ }))



endfunction " }}}
function! s:PluginInitFunction_asyncomplete_emoji() " {{{
    
        au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#emoji#get_source_options({
            \ 'name': 'emoji',
            \ 'allowlist': ['*'],
            \ 'completor': function('asyncomplete#sources#emoji#completor'),
            \ }))


endfunction " }}}

function! s:PluginInitFunction_vim_lsp() " {{{
    " let g:lsp_text_edit_enabled = 0
    " let g:lsp_log_file = expand('~/vim-lsp.log')
    let g:lsp_text_edit_enabled = 1

    let g:lsp_ignorecase = 1

    " let g:enable_lsp_log = 1
    let l:enable_log = get(g:, 'enable_lsp_log', "0")
    if l:enable_log == 1
        let g:lsp_log_verbose = 1
        let g:lsp_log_file = expand('~/lsp.log')
    endif

    function! s:on_lsp_buffer_enabled() abort

        setlocal omnifunc=lsp#complete
        setlocal signcolumn=yes
        if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
        nmap <buffer> gd <plug>(lsp-definition)
        nmap <buffer> gs <plug>(lsp-document-symbol-search)
        nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
        nmap <buffer> gr <plug>(lsp-references)
        nmap <buffer> gi <plug>(lsp-implementation)
        nmap <buffer> ge <plug>(lsp-declaration)
        nmap <buffer> gt <plug>(lsp-type-definition)
        nmap <buffer> <leader>rn <plug>(lsp-rename)
        nmap <buffer> [g <plug>(lsp-previous-diagnostic)
        nmap <buffer> ]g <plug>(lsp-next-diagnostic)
        nmap <buffer> K <plug>(lsp-hover)
        nnoremap <buffer> <expr><c-f> lsp#scroll(+4)
        nnoremap <buffer> <expr><c-d> lsp#scroll(-4)

        nnoremap <buffer> [e :LspNextError<CR>
        nnoremap <buffer> ]e :LspPreviousError<CR>

        nnoremap <buffer> [w :LspNextWarning<CR>
        nnoremap <buffer> ]w :LspPreviousWarning<CR>

        let g:lsp_format_sync_timeout = 1000
    endfunction

    let g:lsp_diagnostics_enabled = 1
    let g:lsp_diagnostics_echo_cursor = 1
    let g:lsp_diagnostics_float_cursor = 1 
	let g:lsp_diagnostics_float_delay = 100
    let g:lsp_diagnostics_virtual_text_enabled = 0
    let g:lsp_diagnostics_virtual_text_wrap = "truncate"
    let g:lsp_diagnostics_signs_error = {'text': '', 'icon': {}}
    let g:lsp_diagnostics_signs_warning = {'text': '', 'icon': {}}

       "\ 'cmd': {server_info->['/mnt/c/Users/taku/.cache/vim-lsp-server/clangd/clangd.exe']},
       "\ 'root_uri': {serveer_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory('compile_config.txt', '.git'))},
    " au User lsp_setup call lsp#register_server({
    "   \ 'name': 'clangd',
    "   \ 'cmd': {server_info->['/mnt/c/Users/taku/.cache/vim-lsp-server/clangd/clangd.exe']},
    "   \ 'root_uri': {serveer_info->lsp#utils#path_to_uri('e:/Project/GR/golem-raiders/')},
    "   \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp', 'cc'],
    "\ })

    augroup lsp_install
      au!
      autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
    augroup END




endfunction " }}}

function! s:PluginInitFunction_vim_lsp_settings() " {{{
     let g:lsp_settings_servers_dir = expand('~/.cache/vim-lsp-server/')

    let g:lsp_settings = {
    \    'pyright-langserver': {
    \        "root_uri_patterns": [
    \            "pyrightconfig.json",
    \        ]
    \    }
    \}

endfunction " }}}


function! s:PluginInitFunction_asyncomplete_ultisnips_vim() " {{{
    call asyncomplete#register_source(asyncomplete#sources#ultisnips#get_source_options({
        \ 'name': 'ultisnips',
        \ 'whitelist': ['*'],
        \ 'completor': function('asyncomplete#sources#ultisnips#completor'),
        \ }))

endfunction " }}}


function! s:PluginInitFunction_echodoc() " {{{
endfunction " }}}



function! s:PluginInitFunction_Align() " {{{
endfunction " }}}


function! s:PluginInitFunction_vim_altr() " {{{
    command! A  call altr#forward()
    command! B  call altr#back()

	" call altr#define('%.h'             , 'Public/%.h'   , 'Classes/%.h'   ,   'Private/%.h'   , 
	"\                '%.cpp'           , 'Public/%.cpp' , 'Classes/%.cpp' ,   'Private/%.cpp' , 
	"\                '%/%.h' , 'Public/%/%.h' , 'Classes/%/%.h' ,   'Private/%/%.h' ,
 "   \                '%/%.cpp'         , 'Public/%/%.cpp','Classes/%/%.cpp' , 'Private/%/%.cpp')

	call altr#define('%.h', '%.cpp',
                   \ 'Public/%.h',
                   \ 'Public/%.cpp',
                   \ 'Classes/GameFramework/%.h',
                   \ 'Classes/GameFramework/%.cpp',
                   \ 'Classes/Engine/%.h',
                   \ 'Classes/Engine/%.cpp',
                   \ 'Classes/%.h',
                   \ 'Classes/%.cpp',
                   \ 'Private/%.h',
                   \ 'Private/%.cpp',
                   \ 'Internal/%.h',
                   \ 'Internal/%.cpp')



endfunctio " }}}





function! s:PluginInitFunction_vim_textmanip() " {{{

    "xmap <C-j> <Plug>(textmanip-move-down)
    "xmap <C-k> <Plug>(textmanip-move-up)
    xmap <C-h> <Plug>(textmanip-move-left)
    xmap <C-l> <Plug>(textmanip-move-right)
endfunction " }}}

function! s:PluginInitFunction_typescript_vim() " {{{
endfunction " }}}

function! s:PluginInitFunction_gundo_vim() " {{{

    nmap <C-F12> :GundoToggle<CR>
    let g:gundo_auto_preview = 0
    let g:gundo_preview_height = 40
endfunction " }}}

function! s:PluginInitFunction_Indent_Guides() " {{{
  " let g:indent_guides_guide_size = 1
  let g:indent_guides_enable_on_vim_startup = 1


    let g:indent_guides_auto_colors = 1
    " autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=red   ctermbg=3
    " autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=green ctermbg=4

    " hi IndentGuidesOdd  guibg=red   ctermbg=3
    " hi IndentGuidesEven guibg=green ctermbg=4

endfunction  "}}}




function! s:PluginInitFunction_vim_visualstar() " {{{

    noremap <Plug>N N
    " noremap * <Plug>(visualstar-*)<Plug>N
    " noremap # <Plug>(visualstar-#)<Plug>N
endfunction "}}}

function! s:PluginInitFunction_project_tar_gz()  " {{{
    let g:proj_flags = "imstvST"
endfunction " }}}

function! s:PluginInitFunction_vim_indent_guides() " {{{
    let g:indent_guides_enable_on_vim_startup = 1
    let g:indent_guides_color_change_percent = 30
    let g:indent_guides_guide_size = 1
    let g:indent_guides_auto_colors = 1
    let g:indent_guides_tab_guides = 0

    " augroup my-vim-indent-setting
    "     autocmd!
    "     autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=#444444  ctermbg=236
    "     autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=#666666  ctermbg=233
    " augroup END

endfunction " }}}

function! s:PluginInitFunction_lightline_vim() " {{{
    "\   'right': [ [ 'lineinfo' ], ['tabstop'], [ 'fileformat', 'fileencoding', 'filetype' ] ]

    let g:lightline = {
                \ 'active': {
                \   'left': [
                \             [ 'mode', 'paste' ],
                \             [ 'fugitive', 'filename', 'anzu', 'lsp_errors', 'lsp_warnings','lsp_status' ] ,
                \          ],
                \   'right': [
                \                [ 'lineinfo' ],
                \                [ 'tabstop' ],
                \                [ 'fileformat', 'fileencoding', 'filetype' ] ,
                \            ]
                \ },
                \ 'tabline': {
                \   'left': [ ['tabs'] ],
                \   'right': [  ['']  ],
                \ },
                \ 'tab' : {
                \ 'active': [ 'tabnum', 'filename', 'modified' ],
                \ 'inactive': [ 'tabnum', 'filename', 'modified' ],
                \},
	            \ 'tab_component_function' : {
                \   'filename': 'LightLineTabFilename',
                \},
                \ 'component_function': {
                \   'fugitive': 'LightLineFugitive',
                \   'filename': 'LightLineFilename',
                \   'fileformat': 'LightLineFileformat',
                \   'filetype': 'LightLineFiletype',
                \   'tabstop': 'LightLineTabStop',
                \   'fileencoding': 'LightLineFileencoding',
                \   'readonly': 'LightLineReadonly',
                \   'modified': 'LightLineModified',
                \   'mode': 'LightLineMode',
                \   'anzu': 'anzu#search_status',
                \   'lsp_errors': 'LightlineLSPErrors',
                \   'lsp_warnings': 'LightlineLSPWarnings',
                \   'lsp_status': 'lightline_lsp_progress#progress',
                \ },
                \ 'component_type': {
                \   'lsp_errors': 'error',
                \   'lsp_warnings': 'warning',
                \   'tab_filename': 'tabsel',
                \ },
                \ }

    let g:lightline.separator = { 'left': "\ue0b0", 'right': "\ue0b2" }
    let g:lightline.subseparator = { 'left': "\ue0b1", 'right': "\ue0b3" }

    let l:colortype = get(g:, 'my_color_scheme', "pencil")
    " 
    if l:colortype == "iceberg_l"
        set background=light
        let  g:lightline.colorscheme = 'iceberg'
    elseif l:colortype == "iceberg_d"
        set background=dark
        let  g:lightline.colorscheme = 'iceberg'
    elseif l:colortype == "hybrid"
        let g:lightline.colorscheme = 'hybrid'
    elseif l:colortype == "pencil"
        let g:lightline.colorscheme = 'pencil_light'
    elseif l:colortype == "PaperColor"
        let g:lightline.colorscheme = 'PaperColor'
    endif
 

    let g:lightline.colorscheme = 'nord'

    function! LightLineModified()
        if &filetype == "help"
            return ""
        elseif &modified
            return "+"
        elseif &modifiable
            return ""
        else
            return ""
        endif
    endfunction

    function! LightLineReadonly()
        if &filetype == "help"
            return ""
        elseif &readonly
            return "\ue0a2"
        else
            return ""
        endif
    endfunction

    function! LightLineTabStop()
        return  &tabstop
    endfunction


    function! LightLineFilename()
        let fname = expand('%:t')
        return fname == '__Tagbar__' ? g:lightline.fname :
                    \ fname =~ '__Gundo\|NERD_tree\|vaffle' ? '' :
                    \ &ft == 'vimfiler' ? vimfiler#get_status_string() :
                    \ &ft == 'unite' ? unite#get_status_string() :
                    \ ('' != LightLineReadonly() ? LightLineReadonly() . ' ' : '') .
                    \ ('' != fname ? fname : '[No Name]') .
                    \ ('' != LightLineModified() ? ' ' . LightLineModified() : '')
    endfunction
    
    function! LightLineTabFilename(n)
          let buflist = tabpagebuflist(a:n)
          let winnr = tabpagewinnr(a:n)
          let _ = expand('#'.buflist[winnr - 1].':.')
          return _ !=# '' ? _ : '[No Name]'
    endfunction

    " let g:writeCount = 0
    function! LightLineFugitive()
        " echomsg 'AA'.g:writeCount
        " let g:writeCount = g:writeCount + 1

        let mark = "\ue702"
        let branch = ''
        "(&ft !~? 'vaffle' || &ft !~? 'vimfiler')
    
            if expand('%:t') !~? 'Tagbar\|Gundo\|NERD' 
                " let mark = "\ue0a0"  " edit here for cool mark
                let branch = FugitiveHead()
            endif

        return branch !=# '' ? mark.branch : ''
    endfunction

    function! LightLineFileformat()
        " return &fileformat
        return winwidth(0) > 70 ? (&fileformat . ' ' . WebDevIconsGetFileFormatSymbol()) : ''
    endfunction

    function! LightLineFiletype()
        " return (&filetype !=# '' ? &filetype : 'no ft')
        return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype . ' ' . WebDevIconsGetFileTypeSymbol() : 'no ft') : ''
    endfunction

    function! LightLineFileencoding()
        let l:temp1 = (&fenc !=# '' ? &fenc : &enc)
        let l:temp2 = &bomb?':BOM':''
        return l:temp1 . l:temp2
    endfunction

    function! LightLineMode()
        let fname = expand('%:t')
        return fname == '__Tagbar__' ? 'Tagbar' :
                    \ fname == 'ControlP' ? 'CtrlP' :
                    \ fname == '__Gundo__' ? 'Gundo' :
                    \ fname == '__Gundo_Preview__' ? 'Gundo Preview' :
                    \ fname =~ 'NERD_tree' ? 'NERDTree' :
                    \ &ft == 'fern' ? 'Fern' :
                    \ &ft == 'vaffle' ? 'Vaffle' :
                    \ &ft == 'unite' ? 'Unite' :
                    \ &ft == 'vimfiler' ? 'VimFiler' :
                    \ winwidth(0) > 60 ? lightline#mode() : ''
    endfunction

    autocmd VimEnter * call SetupLightlineColors()
    function SetupLightlineColors() abort
      let l:pallete = lightline#palette()
      let l:pallete.normal.left[1][3] = 'NONE'
      call lightline#colorscheme()
    endfunction

    let s:palette = g:lightline#colorscheme#{g:lightline.colorscheme}#palette
    let s:palette.normal.middle = [ [ 'NONE', 'NONE', 'NONE', 'NONE' ] ]
    let s:palette.inactive.middle = s:palette.normal.middle
    let s:palette.tabline.middle = s:palette.normal.middle

    function! LightlineLSPWarnings() abort
      return lightline_lsp#warnings()
    endfunction

    function! LightlineLSPErrors() abort
      return lightline_lsp#errors()
    endfunction

    augroup lightlineAutocmd
      autocmd!
      autocmd User lsp_diagnostics_updated call lightline#update()
    augroup END

    "気温が取れないことがあるので最後にとれたのを保存する
    le t s:temperature_min_max = ['-','-']


    function! CtrlPMark()
        if expand('%:t') =~ 'ControlP' && has_key(g:lightline, 'ctrlp_item')
            call lightline#link('iR'[g:lightline.ctrlp_regex])
            return lightline#concatenate([g:lightline.ctrlp_prev, g:lightline.ctrlp_item
                        \ , g:lightline.ctrlp_next], 0)
        else
            return ''
        endif
    endfunction

    let g:ctrlp_status_func = {
                \ 'main': 'CtrlPStatusFunc_1',
                \ 'prog': 'CtrlPStatusFunc_2',
                \ }

    function! CtrlPStatusFunc_1(focus, byfname, regex, prev, item, next, marked)
        let g:lightline.ctrlp_regex = a:regex
        let g:lightline.ctrlp_prev = a:prev
        let g:lightline.ctrlp_item = a:item
        let g:lightline.ctrlp_next = a:next
        return lightline#statusline(0)
    endfunction

    function! CtrlPStatusFunc_2(str)
        return lightline#statusline(0)
    endfunction

    let g:tagbar_status_func = 'TagbarStatusFunc'

    function! TagbarStatusFunc(current, sort, fname, ...) abort
        let g:lightline.fname = a:fname
        return lightline#statusline(0)
    endfunction


    " set laststatus=2

    "let g:unite_force_overwrite_statusline = 0
    let g:vimfiler_force_overwrite_statusline = 0

endfunction " }}}

function! s:PluginInitFunction_caw_vim() " {{{
    nmap <C-n> <Plug>(caw:hatpos:toggle)
    vmap <C-n> <Plug>(caw:hatpos:toggle)
    " nmap <C-n> <Plug>(caw:zeropos:toggle)
    " vmap <C-n> <Plug>(caw:zeropos:toggle)
endfunction " }}}

function! s:PluginInitFunction_errormarker_vim() " {{{

    let errormarker_disablemappings = 1
endfunction "}}}

function! s:PluginInitFunction_vim_anzu() " {{{

    "anzu
    if s:is_installed('lightline.vim') 
        nmap n <Plug>(anzu-n)
        nmap N <Plug>(anzu-N)
        nmap * <Plug>(anzu-star)
        nmap # <Plug>(anzu-sharp)
    else
        nmap n <Plug>(anzu-n-with-echo)
        nmap N <Plug>(anzu-N-with-echo)
        nmap * <Plug>(anzu-star-with-echo)
        nmap # <Plug>(anzu-sharp-with-echo)
    endif
    " clear status
    "nmap <Esc><Esc> <Plug>(anzu-clear-search-status)
    nmap <silent> <ESC><ESC> :<C-u>nohlsearch<CR><Plug>(anzu-clear-search-status)
endfunction " }}}


function! s:PluginInitFunction_ShowMarks() " {{{
    " let g:showmarks_include = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    function! s:my_showmarks_highlight() 
        highlight clear SignColumn
        " highlight! link Comment SignColumn 

        highlight! ShowMarksHLl ctermfg=12 cterm=bold guifg=#81a2be gui=bold
        highlight! ShowMarksHLu ctermfg=12 cterm=bold guifg=#81a2be gui=bold
        highlight! ShowMarksHLo ctermfg=12 cterm=bold guifg=#81a2be gui=bold
        highlight! ShowMarksHLm ctermfg=12 cterm=bold guifg=#81a2be gui=bold

    endfunction

    augroup my-showmarks-setting
        autocmd!
        autocmd VimEnter,WinEnter *
                    \ call s:my_showmarks_highlight()
    augroup END
endfunction " }}}


function! s:PluginInitFunction_colors() " {{{

    if ( exists('g:color_changed') && g:color_changed )
    else

        let g:color_changed=1

        let l:colortype = get(g:, 'my_color_scheme', "iceberg_l")
        "let l:colortype = "pencil"
        "let l:colortype = "iceberg_d"
        " let l:colortype = "iceberg_l"
        "


        if l:colortype == "iceberg_d"
            set background=dark
            let g:iceberg_term_colors = 1
            syntax on
            colorscheme iceberg

            hi! Search gui=bold cterm=bold term=bold guifg=#B4B282 guibg=#5B7811

        elseif l:colortype == "iceberg_l"
            set background=light
            let g:iceberg_term_colors = 1
            let g:transparent = 1
            syntax on
            colorscheme iceberg
        elseif l:colortype == "hybrid"
            if !has("gui_running")
            endif
            let g:hybrid_reduced_contrast = 1
            set background=dark
            colorscheme hybrid 
            syntax on

        elseif l:colortype == "pencil"
            set background=light
            syntax on
            colorscheme pencil
            let g:pencil_higher_contrast_ui = 1

            if !has('gui_running')
                let g:pencil_terminal_italics = 1
                let g:pencil_spell_undercurl = 0
            endif

             hi Search guifg=#E06030 ctermfg=233
             hi LineNr guifg=#999999 ctermfg=243
             hi CursorLineNr guifg=#444444 ctermfg=232
        elseif l:colortype == "brise"
            set background=light
            " let g:iceberg_term_colors = 1
            syntax on
            colorscheme brise

        elseif l:colortype == "cobalt2"
            syntax on
            colorscheme cobalt2
        else

            let g:PaperColor_Theme_Options = {
              \   'theme': {
              \     'default': {
              \       'allow_bold': 0,
              \       'allow_italic': 0
              \     }
              \   }
              \ }
            set background=light
            syntax on
            colorscheme PaperColor

        endif



        "設定そのまま
        function! s:color_setting_from_transparent() abort
            hi Normal ctermbg=NONE guibg=NONE
            hi NonText ctermbg=NONE guibg=NONE
            hi EndOfBuffer ctermbg=NONE guibg=NONE
            hi LineNr cterm=NONE ctermbg=NONE guibg=NONE

            hi TablineSel ctermbg=NONE guibg=NONE
            hi ColorColumn ctermbg=NONE guibg=NONE
            hi CursorColumn ctermbg=NONE guibg=NONE
            hi CursorLineNr cterm=NONE gui=NONE guifg=NONE guibg=NONE ctermfg=NONE ctermbg=NONE
            hi CursorLine cterm=NONE gui=NONE guifg=NONE guibg=NONE ctermfg=NONE ctermbg=NONE

            " hi Error cterm=NONE gui=NONE guifg=NONE guibg=NONE ctermfg=NONE ctermbg=NONE



            if s:is_installed('vim-easymotion')
                hi! link EasyMotionTarget ErrorMsg
                hi! link EasyMotionShade  Comment

                hi! link EasyMotionTarget2First MatchParen
                hi! link EasyMotionTarget2Second MatchParen

                hi! link EasyMotionMoveHL Search
                hi! link EasyMotionIncSearch Search
            endif


            if s:is_installed('fuzzy-motion.vim')
                hi! default FuzzyMotionShade   ctermfg=grey ctermbg=NONE cterm=NONE           guibg=NONE    guifg=#777777 gui=NONE
                hi! default FuzzyMotionChar    ctermfg=209  ctermbg=NONE cterm=underline,bold guifg=#E27878 guibg=NONE    gui=underline,bold
                hi! default FuzzyMotionSubChar ctermfg=209  ctermbg=NONE cterm=underline,bold guifg=#FFAF60 guibg=NONE    gui=NONE
            endif

        endfunction
        "     
        
        let l:transparent = get(g:, 'transparent', "0")

        if l:transparent == 1 && !has('gui_running')
            call s:color_setting_from_transparent()

            augroup  transparent_gb
                autocmd!
                autocmd ColorScheme * call s:color_setting_from_transparent()
            augroup END
        endif

    endif

endfunction " }}}

function! s:PluginInitFunction_iceberg_vim() " {{{
    set background=dark
    "highlight! LineNr ctermfg=8 guifg=#637180

    let g:iceberg_term_colors = 1
    syntax on
    colorscheme iceberg
    "hi! Normal ctermbg=NONE guibg=NONE
    "hi! NonText ctermbg=NONE guibg=NONE


endfunction " }}}



function! s:PluginInitFunction_ctrlp_vim() " {{{

    set wildignore=*.o,*.obj,*.la,*.lo,*.so,*.pyc,*.pyo,*.jpg,*.png,*.gif,*.xlsx,*.tga,*.JPEG,*.meta,*.fbx,*.mp4,*.aep,*.mb
                \,*.pptx,*.psd,*.dll,*.pdb,*.xlsm,*.avi,*.deploy,*.vsto,*.mat,*.prefab,*.controller,*.anim
                \,exe

    let g:ctrlp_custom_ignore = {
      \ 'dir':  '\v[\/](\.git|\.hg|\.svn|\.bundle|\.vscode|\.idea|cmake-build-debug|build|node_module)$',
      \ 'file': '\v(\.exe|\.so|\.dll|\.DS_Store|\.meta|\.pdf|\.md5|\.asset|\.SyeleCop|\.mdb|\.xlsx)$',
      \ 'link': '',
      \ }
  
    let g:ctrlp_max_files = 0
  
    let g:ctrlp_working_path_mode = 'ra'

    let g:ctrlp_cache_dir = $HOME.'/.cache/ctrlp'
    "let g:ctrlp_clear_cache_on_exit = 0
    "let g:ctrlp_lazy_update = 1
    "let g:ctrlp_regexp = 1
    "let g:ctrlp_regexp = 0
  
    let g:ctrlp_map = '<Nop>'
 
    " nremap <silent> <C-p> :<C-u>call <SID>ctrlp_file_async()<CR>
    " noremap <silent> <C-b> :<C-u>CtrlPBuffer<CR>
    noremap <silent> <C-g> :<C-u>CtrlPMRU<CR>
    "noremap <silent> <C-m> :<C-u>CtrlPMark<CR>
  

    let g:ctrlp_extensions = ['tag', 'quickfix', 'dir', 'changes', 'mixed']
-    "let g:ctrlp_extensions = ['tag', 'quickfix', , 'line', 'mixed']
    


    function! s:ctrlp_file_async()
        let l:current_path = expand('%:p:h')
-
        if isdirectory(l:current_path.'/.git')
            execute 'CtrlP'
        else
            execute 'CtrlPCurWD'
        endif
    endfunction

    " if !has('gui_running') && s:is_installed('cpsm')
    "     " let g:cpsm_unicode=1
    "     let g:cpsm_highlight_mode='detailed'
    "     let g:ctrlp_match_func = {'match': 'cpsm#CtrlPMatch'}
    " endif

    " if executable('files')
    "     let g:ctrlp_user_command='files -a %s'
    " endif

    " if executable('ag')
    "   let g:ctrlp_user_command='ag %s --ignore-dir ".git" --ignore "*.meta" --nocolor --nogroup -g ""'
    " endif
endfunction " }}}


function! s:PluginInitFunction_memolist_vim() " {{{

    let g:memolist_memo_suffix = "md"
    let g:memolist_path = expand('$HOME/memo/_posts')

    let g:memolist_fzf = 1 
    let g:memolist_ex_cmd = 'Clap files'
endfunction " }}}

function! s:PluginInitFunction_vim_clap() " {{{

    " let g:clap_builtin_fuzzy_filter_threshold = 10
    " let g:clap_cache_threshold = 10000000
	"

	let g:clap_disable_run_rooter = v:true

    let g:clap_layout = { 'relative': 'editor' }
    let g:clap_preview_direction = 'UD'

    let g:clap_provider_grep_executable = 'rg'

    let g:GitCommand = 'git'
    if has('win32') == 1 || has('win64') == 1
        let g:GitCommand = 'git.exe'
    elseif system('uname -r') =~ "Microsoft"
        let g:GitCommand = 'git.exe'
    endif

    " function! s:clap_on_move() abort
    "     call clap#preview#file(g:clap.display.getcurline())
    " endfunction
    "
    let g:clap_disable_run_rooter = v:true

    autocmd VimEnter,Colorscheme * :hi ClapDisplay  guifg=red   ctermfg=3
    function! s:clap_git_ls_files() abort
        " let l:current_path = expand('%:h')
        redir =>  message
        execute 'pwd'
        redir END
        let l:current_path = message.'/.git'
        if has('win32') == 1 || has('win64') == 1
            let l:current_path = substitute(l:current_path, '\n','', "g")
            let l:current_path = substitute(l:current_path, '/','\', "g")
        endif
        if isdirectory(l:current_path)
            :Clap git_ls_files
        else
            :Clap files
        endif
    endfunction

      
    let s:rg_with_type = {}

    function! s:rg_with_type.source() abort
      call clap#rooter#try_set_cwd()
      let l:commands = 'rg --files --type cpp --type csharp --type config --type shader'
      return l:commands
    endfunction

    let s:rg_with_type.sink = function('clap#provider#files#sink_impl')
    let s:rg_with_type['sink*'] = function('clap#provider#files#sink_star_impl')
    let s:rg_with_type.on_move = function('clap#provider#files#on_move_impl')
    let s:rg_with_type.on_typed = { -> clap#client#notify_provider('on_typed') }
    let s:rg_with_type.icon = 'File'


    let g:clap#provider#rg_with_type# = s:rg_with_type

    noremap <silent> <C-p> :Clap files<CR>
    noremap <silent> <C-g> :Clap git_files<CR>
    noremap <silent> <C-s> :Clap rg_with_type<CR>

    noremap <silent> <leader>h :Clap history<CR>
    noremap <silent> <leader>f :Clap blines<CR>
    noremap <silent> <leader>b :Clap buffers<CR>
    noremap <silent> <leader>r :Clap grep<CR>
    noremap <silent> re :Clap recent_files<CR>


endfunction " }}}

function! s:PluginInitFunction_fzf_vim() " {{{



    let $FZF_DEFAULT_COMMAND = 'rg --files'

    function! s:all_files()
      return extend(
     \ filter(copy(v:oldfiles),
     \        "v:val !~ 'fugitive:\\|NERD_tree\\|^/tmp/\\|.git/'"),
     \ map(filter(range(1, bufnr('$')), 'buflisted(v:val)'), 'bufname(v:val)'))
    endfunction

    command! -bang -nargs=? -complete=dir Files
      \ call fzf#vim#files(<q-args>, fzf#vim#with_preview({
      \ 'source': 'rg -t cpp -t config -t csharp --files --hidden '}), <bang>0)


    " let g:GitCommand = 'git'
    " if has('win32') == 1 || has('win64') == 1
    "     let g:GitCommand = 'git.exe'
    " elseif system('uname -r') =~ "Microsoft"
    "     let g:GitCommand = 'git.exe'
    " endif
    " 
    " command! -bang -nargs=? -complete=dir GitFiles
    "  \ call fzf#vim#files(<q-args>, fzf#vim#with_preview({
    "  \ 'source': g:GitCommand.' ls-files -co --exclude-standard "*.c" "*.cpp" "*.h" "*.hpp" "*.cs" "*.usf" "*.ush" "*.inl" "*.ini" '}), <bang>0)
    " 
    " command! -bang -nargs=? -complete=dir GitFilesWithSub
    "  \ call fzf#vim#files(<q-args>, fzf#vim#with_preview({
    "  \ 'source': g:GitCommand.' ls-files --recurse-submodules -c --exclude-standard "*.c" "*.cpp" "*.h" "*.hpp" "*.cs" "*.usf" "*.ush" "*.inl" "*.ini" '}), <bang>0)
    " 
    " command! -bang -nargs=? -complete=dir GitStatus
    "  \ call fzf#vim#files(<q-args>, fzf#vim#with_preview({
    "  \ 'source': g:GitCommand.' ls-files -om --exclude-standard "*.c" "*.cpp" "*.h" "*.hpp" "*.cs" "*.usf" "*.ush" "*.inl" "*.ini" '}), <bang>0)
    " 
    " 
    
    " function! OpenFileFromGitFiles(query, bang)
    "     let s:file_path = expand("<cfile>")
    "     if s:file_path == ''
    "         return 0
    "     endif
    " 
    "     call fzf#vim#files(a:query, fzf#vim#with_preview({
    "                    \ 'source': g:GitCommand.' ls-files --recurse-submodules -c --exclude-standard "*.c" "*.cpp" "*.h" "*.hpp" "*.cs" "*.usf" "*.ush" "*.inl" "*.ini" ',
    "                    \ 'options': '--select-1 --query='.s:file_path
    "                    \}), a:bang)
    " endfunction
    " command! -bang -nargs=? -complete=dir OGitFileFzf call OpenFileFromGitFiles(<q-args>, <bang>0)
    " 
    " 
    " function! RipgrepFzf(query, fullscreen)
    "   let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case %s || true'
    "   let initial_command = printf(command_fmt, shellescape(a:query))
    "   let reload_command = printf(command_fmt, '{q}')
    "   let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
    "   if has('win32') || has('win64') || has('win32unix')
    "     call fzf#vim#grep(initial_command, 1,  a:fullscreen)
    "   else
    "     call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
    "   endif
    " endfunction
    " 
    " command! -nargs=* -bang Rg call RipgrepFzf(<q-args>, <bang>0)
    " 
    " function! GitGrepFzf(query, fullscreen)
    "   let command_fmt = g:GitCommand.' grep --line-number --no-color %s -- "*.cpp" "*.h" "*.cs" "*.usf" "*.ush" "*.ini" || true'
    "   let initial_command = printf(command_fmt, shellescape(a:query))
    "   let reload_command = printf(command_fmt, '{q}')
    "   let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
    "   if has('win32') || has('win64') || has('win32unix')
    "     call fzf#vim#grep(initial_command, 1,  a:fullscreen)
    "   else
    "     call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
    "   endif
    " endfunction
    " 
    " command! -nargs=* -bang GitGrep call GitGrepFzf(<q-args>, <bang>0)

    " if has('win32') == 1 || has('win64') == 1
    "     let g:fzf_layout = { 'down': '~40%' }
    "     let g:fzf_preview_window = []
    " else
    "     let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6,  'border': 'sharp' } }
    " endif
   
    " if has('win32') == 1 || has('win64') == 1
    "     let $PATH= "C:\\Program\ Files\\Git\\bin\\bash.exe".$PATH
    " endif
    " let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6,  'border': 'sharp' } }

    " function! s:build_quickfix_list(lines)
    "   call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
    "   copen
    "   cc
    " endfunction

    " let g:fzf_action = {
    " \ 'ctrl-q': function('s:build_quickfix_list'),
    " \ 'ctrl-t': 'tab split',
    " \ 'ctrl-x': 'split',
    " \ 'ctrl-v': 'vsplit' }

    " let g:fzf_preview_window = []


    noremap <silent> <C-p> :Files<CR>
    " noremap <silent> <C-g> :GitFiles<CR>
    " noremap <silent> <leader>gr :GitFilesWithSub<CR>
    " noremap <silent> <leader>gf :OGitFileFzf<CR>

    " noremap <silent> <C-s> :GitStatus<CR>
    " noremap <silent> <leader>h :History<CR>
    " noremap <silent> <leader>f :BLines<CR>
    " noremap <silent> <leader>b :Buffers<CR>
    " noremap <silent> <leader>r :Rg<CR>




endfunction " }}}

function! s:PluginInitFunction_vimtweak_vim() " {{{
    
        autocmd guienter * call libcallnr("vimtweak64.dll","SetAlpha",225)

endfunction " }}}


function! s:PluginInitFunction_asyncrun_vim() " {{{
    augroup MyAsyncRunAutoCopen
        autocmd!
        autocmd User AsyncRunStart call asyncrun#quickfix_toggle(8, 1)
    augroup END 


    function! Rgrep(...)
      if a:0 >= 1
        let l:command = ['AsyncRun rg --vimgrep --no-heading ']
        call extend(l:command,a:000)
        let result  = join(l:command)
        execute result
      else
        echo "please set args"
      end
    endfunction
    command! -nargs=* Rgrep call Rgrep(<f-args>)

    function! GRgrep(...)
      if a:0 >= 1
        let l:command = ['AsyncRun git grep --line-number --no-color']
        call extend(l:command,a:000)
        let result  = join(l:command).' -- "*.cpp" "*.h" "*.cs" "*.usf" "*.ush" "*.ini" '
        execute result
      else
        echo "please set args"
      end
    endfunction
    command! -nargs=* Rgrep call Rgrep(<f-args>)
    command! -nargs=* GRgrep call GRgrep(<f-args>)

endfunction " }}}


function! s:PluginInitFunction_vim_ripgrep() " {{{

    command! -nargs=? -complete=file Ripgrep call ripgrep#search(<q-args>)
    command! -nargs=+ -complete=file RipgrepSenitive :call ripgrep#search("-i ".<q-args>)

endfunction " }}}




function! s:PluginInitFunction_vim_qf_preview() " {{{
    augroup qfpreview
        autocmd!
        autocmd FileType qf nmap <buffer> p <plug>(qf-preview-open)
    augroup END

    let g:qfpreview = {
        \ 'top': 'g',
        \ 'bottom': 'G',
        \ 'scrollup': 'k',
        \ 'scrolldown': 'j',
        \ 'halfpageup': 'u',
        \ 'halfpagedown': 'd',
        \ 'fullpageup': 'b',
        \ 'fullpagedown': 'f',
        \ 'next': 'n',
        \ 'previous': 'b',
        \ 'number': 1,
        \ 'sign': {
        \     'text': '>>',
        \      'texthl': 'Todo'
        \  }
        \ }

endfunction " }}}

function! s:PluginInitFunction_editorconfig_vim() " {{{
endfunction " }}}

function! s:PluginInitFunction_vim_surround() "{{{



    nmap <Leader>{ ysiw}
    nmap <Leader>} ysiw{
    nmap <Leader>[ ysiw]
    nmap <Leader>] ysiw[
    nmap <Leader>( ysiw)
    nmap <Leader>) ysiw(
    nmap <Leader>< ysiw>
    nmap <Leader>> ysiw<
    nmap <Leader>" ysiw"
    nmap <Leader>' ysiw'
    nmap <Leader>` ysiw`
    nmap <Leader>* ysiw*
    xmap { S}
    xmap } S{
    xmap [ S]
    xmap ] S[
    xmap ( S)
    xmap ) S(
    xmap < S>
    xmap > S<
    xmap " S"
    xmap ' S'
    xmap ` S`
    xmap * S*
endfunction "}}}

function! s:PluginInitFunction_vim_sandwich() "{{{

    nmap s <Nop>
    xmap s <Nop>

    nmap <Leader>{ saiw}
    nmap <Leader>} saiw{
    nmap <Leader>[ saiw]
    nmap <Leader>] saiw[
    nmap <Leader>( saiw)
    nmap <Leader>) saiw(
    nmap <Leader>< saiw>
    nmap <Leader>> saiw<
    nmap <Leader>" saiw"
    nmap <Leader>' saiw'
    nmap <Leader>` saiw`
    nmap <Leader>* saiw*


    xmap { sa}
    xmap } sa{
    xmap [ sa]
    xmap ] sa[
    xmap ( sa)
    xmap ) sa(
    xmap < sa>
    xmap > sa<
    xmap " sa"
    xmap ' sa'
    xmap ` sa`
    xmap * sa*
endfunction "}}}

function! s:PluginInitFunction_vim_operator_replace() "{{{

    nmap _ <Plug>(operator-replace)
endfunction "}}}

function! s:PluginInitFunction_supertab() " {{{

    let g:SuperTabDefaultCompletionType = "<c-n>"
endfunction " }}}

function! s:PluginInitFunction_vim_quickrun() " {{{

    " if exists("quickrun_running") || !exists("g:quickrun_config")
    "     let g:quickrun_config = {}
    " endif
    let g:quickrun_config = {
               \    "_" : {
               \        "outputter/buffer/split" : ":botright 8sp",
               \        "outputter/buffer/running_mark" : "ﾊﾞﾝ（∩`･ω･）ﾊﾞﾝﾊﾞﾝﾊﾞﾝﾊﾞﾝﾞﾝ",
               \        "outputter/quickfix/open_cmd" : "copen 8",
               \        "runner" : "job",
               \        "runner/job/interval" : 60,
               \        "hook/close_buffer/enable_failure" : 0,
               \        "hook/close_quickfix/enable_success" : 0,
               \        "hook/close_buffer/enable_success" : 0,
               \        "hook/close_buffer/enable_empty_data" : 0,
               \        "hook/close_unite_quickfix/enable_module_loaded" : 0,
               \        "hook/nuko/enable" : 1,
               \        "hook/sweep/enable" : 0,
               \        "hook/extend_config/enable" : 1,
               \        "hook/extend_config/force" : 1,
               \        "hook/echo/enable" : 1,
               \        "hook/echo/enable_output_exit" : 1,
               \        "hook/echo/priority_exit" : 10000,
               \        "hook/echo/output_success" :"｡:+.゜ヽ(*′ω`)ﾉﾞ｡:+.゜ﾔｯﾀｰ",
               \        "hook/echo/output_failure" : "(∪´;ﾟ;ω;ﾟ)･;'.､･;'.･;';ﾌﾞﾌｫ",
               \        "hook/clear_quickfix/enable_hook_loaded" : 1,
               \        "hook/clear_location_list/enable_hook_loaded" : 1,
               \        "hook/hier_update/enable_exit" : 1,
               \        "hook/quickfix_stateus_enable/enable_exit" : 1,
               \        "hook/quickfix_replate_tempname_to_bufnr/enable_exit" : 1,
               \        "hook/quickfix_replate_tempname_to_bufnr/priority_exit" : -10,
               \        "hook/quickrunex/enable" : 1,
               \        "hook/back_tabpage/enable_exit" : 0,
               \        "hook/back_tabpage/priority_exit" : -2000,
               \        "hook/back_window/enable_exit" : 0,
               \        "hook/back_window/priority_exit" : -1000,
               \        "hook/unite_quickfix/enable" : 0,
               \    },
               \    "run/job" : {
               \        "exec": "%s:p:r %a",
               \        "runner" : "job",
               \        "runner/job/interval" : 20,
               \        "hook/output_encode/encoding" : "utf-8",
               \        "hook/failure_close_buffer/enable" : 0,
               \        "hook/extend_config/enable" : 0,
               \        "hook/close_buffer/enable_empty_data" : 1,
               \        "hook/close_buffer/enable_exit" : 0,
               \        "hook/close_buffer/enable_failure" : 0,
               \        "hook/unite_quickfix/enable" : 0,
               \    },
               \}

    " call extend(g:quickrun_config, s:config)
    " unlet s:config

    function! s:remake(commandType)
        let command = "QuickRun " . a:commandType
        if g:lastMakeFilePath != ""
            let command = command." -args ".g:lastMakeFilePath
        endif
        execute command
    endfunction

    function! s:search_file(fname)
        let path = expand('%:p:h')
        let target = ""
        let dir = fnamemodify(path, ':p:h')
        let updir = ''
        while dir !=# updir
            let target = s:match_files(dir, a:fname)
            if target != ''
                break
            endif
            let updir = dir
            let dir = fnamemodify(dir, ':h')
            if (has('win32') || has('win64')) && dir =~ '^\\\\[^\\]\+$'
                break
            endif
        endwhile
        return target
    endfunction

    function! s:match_files(path, fname)
      let path = escape(a:path, '*?[,')
      let files = map(split(globpath(path, a:fname), "\n"), 'fnamemodify(v:val, ":t")')
      let files = filter(map(files, 'a:path . "/" . v:val'), 'filereadable(v:val)')
      if len(files) == 0
          return ''
      endif
      return l:files[0]
    endfunction

    let s:search_make_hook ={
               \ "name" : "searchmake",
               \ "kind" : "hook",
               \ "config" : {
               \ "enable" : 0,
               \ "targetname" : "Makefile",
               \},
               \}


    function! s:search_make_hook.init(session)
        if has_key(a:session.config, "hook/searchmake/targetname") == 0
            return
        endif
        let l:target = a:session.config["hook/searchmake/targetname"]
        let l:path = s:search_file(l:target)

        let a:session.config["hook/cd/directory"] = fnamemodify(l:path, ":p:h")
        let a:session.config.args = l:path

    endfunction

    call quickrun#module#register(s:search_make_hook, 1)

    let s:cpp_config = {
               \    "cpp" : {
               \        "type" : "cpp/clang++",
               \        "hook/extend_config/enable" : 0,
               \        "hook/close_buffer/enable_exit" : 0,
               \    },
               \    "cpp/_" : {
               \        "hook/quickrunex/enable" : 0,
               \        "hook/close_buffer/enable_exit" : 0,
               \        "subtype" : "run/job",
               \        "outputter/quickfix/errorformat" : "%f:%l:%c: %t%*[^:]:%m",
               \        "hook/unite_quickfix/enable" : 0,
               \    },
               \    "cpp/clang++" : {
               \        "command"   : "clang++",
               \        "cmdopt"    : "-std=c++11 -Wall -Wno-pragma-once-outside-header",
               \    },
               \    "cpp/make": {
               \        "command"   : "make",
               \        "cmdopt"   : "-j4",
               \        "exec"   : "%c -%o",
               \        "outputter/quickfix/errorformat" : "%f:%l:%c: %t%*[^:]:%m",
               \        "hook/searchmake/enable" : 1,
               \        "hook/searchmake/targetname" : "Makefile",
               \    },
               \    "cpp/qmake": {
               \        "command"   : "make",
               \        "cmdopt"   : "-j4",
               \        "exec"   : "%c -%o",
               \        "outputter/quickfix/errorformat" : "%f:%l:%c: %t%*[^:]:%m",
               \        "hook/searchmake/enable" : 1,
               \        "hook/searchmake/targetname" : "Makefile",
               \    },
               \    "cpp/cmake": {
               \        "command"   : "make",
               \        "cmdopt"   : "-j4",
               \        "exec"   : "%c -%o",
               \        "outputter/quickfix/errorformat" : "%f:%l:%c: %t%*[^:]:%m",
               \        "hook/searchmake/enable" : 1,
               \        "hook/searchmake/targetname" : "Makefile",
               \    },
               \}

    :command! Reqmake :call s:remake("cpp/qmake")
    :command! Recmake :call s:remake("cpp/cmake")
    :command! Remake :call s:remake("cpp/make")

    call extend(g:quickrun_config, s:cpp_config)
    unlet s:cpp_config

    let s:cs_config = {
               \    "cs" : {
               \        "type" : "cs/make",
               \        "hook/extend_config/enable" : 0,
               \        "hook/close_buffer/enable_exit" : 0,
               \    },
               \    "cs/_" : {
               \        "hook/quickrunex/enable" : 0,
               \        "hook/close_buffer/enable_exit" : 0,
               \        "subtype" : "run/job",
               \        "outputter" : "quickfix",
               \        "outputter/quickfix/errorformat" : "\ %#%f(%l\\\,%c):\ %m",
               \        "hook/unite_quickfix/enable" : 0,
               \    },
               \    "cs/make": {
               \        "command"   : "msbuild",
               \        "cmdopt"   : "/nologo /v:q /p:GenerateFullPaths=true",
               \        "exec"   : "%c %o %a ",
               \        "outputter" : "quickfix",
               \        "outputter/quickfix/errorformat" : "\ %#%f(%l\\\,%c):\ %trror\ \%m,\ %#%f(%l\\\,%c):\ %tarning\ \%m,\ %#%f(%l\\\,%c):\ %m",
               \        "hook/searchmake/enable" : 1,
               \        "hook/searchmake/targetname" : "*.sln",
               \    },
               \}
    call extend(g:quickrun_config, s:cs_config)
    unlet s:cs_config
endfunction " }}}


function! s:PluginInitFunction_vim_hier() " {{{

    execute "highlight hunder_curl cterm=underline ctermfg=Red ctermbg=NONE gui=undercurl guisp=Red guifg=Red guibg=NONE"

    let g:hier_highlights = {
                \"make" : {
                \   'qf' : {
                \       "error" : "hunder_curl",
                \       "warning" : "Todo",
                \   },
                \},
                \}

    nnoremap <ESC>h :HierClear<CR>
endfunction " }}}

function! s:PluginInitFunction_vim_watchdogs() " {{{

    let g:watchdogs_check_BufWritePost_enable = 0
    let g:watchdogs_check_CursorHold_enable = 0

    function! Disable_watchdogs()
        let g:watchdogs_check_BufWritePost_enable = 0
        let g:watchdogs_check_CursorHold_enable = 0
    endfunction

    function! Enable_watchdogs()
        let g:watchdogs_check_BufWritePost_enable = 1
        let g:watchdogs_check_CursorHold_enable = 1
    endfunction

    function! Enable_qtcpp_watchdogs()
        let g:quickrun_config = get(g:, 'quickrun_config', {})
        if has('mac')
            let g:quickrun_config["cpp/watchdogs_checker"]["type"] = "watchdogs_checker/qt_osx_clang++"
        else
        endif
        call watchdogs#setup(g:quickrun_config)

    endfunctio

    function! Disable_qtcpp_watchdogs()

        let g:quickrun_config = get(g:, 'quickrun_config', {})
        let g:quickrun_config["cpp/watchdogs_checker"]["type"] = "watchdogs_checker/clang++"
        call watchdogs#setup(g:quickrun_config)

    endfunctio

    let g:quickrun_config = get(g:, 'quickrun_config', {})

    let s:config = {
                \"watchdogs_checker/_" : {
                \   "runner" : "job",
                \   "runner/job/interval" : "40",
                \ },
                \"cpp/watchdogs_checker" : {
                \	"type" : "watchdogs_checker/clang++",
                \},
                \"watchdogs_checker/clang++" : {
                \	"exec"   : "%c -%o %s:p ",
                \	"cmdopt" : "-std=c++11 -Wall -Wno-pragma-once-outside-header -fsyntax-only",
                \   "quickfix/errorformat" : "%f:%l:%c:\ %t%s:\ %m",
                \},
                \"watchdogs_checker/qt_osx_clang++" : {
                \	"exec"   : "%c -%o %s:p ",
                \	"cmdopt" : "-std=gnu++11 -Wall -Wno-pragma-once-outside-header -fsyntax-only",
                \   "quickfix/errorformat" : "%f:%l:%c:\ %t%s:\ %m",
                \},
                \}

    call extend(g:quickrun_config, s:config)
    unlet s:config

    call watchdogs#setup(g:quickrun_config)
endfunction " }}}

function! s:PluginInitFunction_calendar_vim() " {{{
endfunction " }}}

function! s:PluginInitFunction_vim_zenspace() " {{{
endfunction " }}}

function! s:PluginInitFunction_vim_showmarks() " {{{
endfunction " }}}


function! s:PluginInitFunction_open_browser_vim() " {{{
    let g:netrw_nogx = 1
    nmap gx <Plug>(openbrowser-smart-search)
    vmap gx <Plug>(openbrowser-smart-search)
endfunction " }}}

function! s:PluginInitFunction_omnisharp_vim() " {{{
    
    let g:OmniSharp_timeout = 2

    let g:OmniSharp_selector_ui = 'fzf'
    let g:OmniSharp_server_type = 'roslyn'
    let g:omnicomplete_fetch_full_documentation=0
    let g:OmniSharp_typeLookupInPreview=0

    let g:OmniSharp_server_use_mono=1
    let g:OmniSharp_server_stdio = 1
    " let g:OmniSharp_server_stdio_quickload = 1

    " let g:OmniSharp_start_server = 1
    let g:OmniSharp_want_snippet = 1
   " let g:OmniSharp_loglevel = 'debug'
    " let g:OmniSharp_diagnostic_overrides = {
    "    \ 'IDE0010': {'type': 'I'},
    "    \ 'IDE0055': {'type': 'W', 'subtype': 'Style'},
    "    \ 'CS8019': {'type': 'None'},
    "    \ 'RemoveUnnecessaryImportsFixable': {'type': 'None'}
    "\}

    let g:OmniSharp_highlight_types = 2

    if s:is_installed('ale')
        let g:ale_linters = { 'cs': ['OmniSharp'] }
    endif

    let g:OmniSharp_popup = 1
    let g:OmniSharp_popup_options = {
    \ 'highlight': 'Normal',
    \ 'padding': [1],
    \ 'border': [1]
    \}

    augroup omnisharp_commands
        autocmd!

        autocmd FileType cs nmap <silent> <buffer> <Leader>fi <Plug>(omnisharp_find_implementations)
        autocmd FileType cs nmap <silent> <buffer> <Leader>fu <Plug>(omnisharp_find_usages)
        autocmd FileType cs nmap <silent> <buffer> <Leader>fs <Plug>(omnisharp_find_symbol)

        autocmd FileType cs nmap <silent> <buffer> <Leader>fd <Plug>(omnisharp_go_to_definition)
        autocmd FileType cs nmap <silent> <buffer> <Leader>fpd <Plug>(omnisharp_preview_definition)
        autocmd FileType cs nmap <silent> <buffer> <Leader>fpi <Plug>(omnisharp_preview_implementations)

        autocmd FileType cs nmap <silent> <buffer> <Leader>fr <Plug>(omnisharp_rename)

        autocmd FileType cs nmap <silent> <buffer> <Leader>fx <Plug>(omnisharp_fix_usings)
        autocmd FileType cs nmap <silent> <buffer> <Leader>tt <Plug>(omnisharp_type_lookup)
        autocmd FileType cs nmap <silent> <buffer> <Leader>dc <Plug>(omnisharp_documentation)

        autocmd FileType cs nmap <silent> <buffer> <C-\> <Plug>(omnisharp_signature_help)

        autocmd FileType cs nmap <silent> <buffer> <C-k> <Plug>(omnisharp_navigate_up)
        autocmd FileType cs nmap <silent> <buffer> <C-j> <Plug>(omnisharp_navigate_down)

        autocmd FileType cs nmap <silent> <buffer> <Leader>cc <Plug>(omnisharp_global_code_check)

        autocmd FileType cs nmap <silent> <buffer> <Leader>ors <Plug>(omnisharp_restart_all_servers))

    augroup END

    nnoremap <Leader><Space> :OmniSharpGetCodeActions<CR>
    xnoremap <Leader><Space> :call OmniSharp#GetCodeActions('visual')<CR>



endfunction " }}}

function! s:PluginInitFunction_ale() " {{{


    " Map movement through errors without wrapping.
    nmap <silent> wn <Plug>(ale_next)
    nmap <silent> wp <Plug>(ale_previous)
    " OR map keys to u>se wrapping.
    nmap <silent> w[ <Plug>(ale_next_wrap)
    nmap <silent> w] <Plug>(ale_previous_wrap)


    let g:ale_fix_on_save = 0

endfunction " }}}

function! s:PluginInitFunction_vim_easymotion() " {{{

    " let g:EasyMotion_move_highlight = 1
    " let g:EasyMotion_inc_highlight = 1
    " let g:EasyMotion_landing_highlight = 1
    let g:EasyMotion_do_mapping = 0
    let g:EasyMotion_smartcase = 1

    nmap <Leader>s <Plug>(easymotion-s2)
    nmap <Leader>f <Plug>(easymotion-overwin-f2)

    nmap <Leader>j <Plug>(easymotion-j)
    nmap <Leader>k <Plug>(easymotion-k)
    "
    " Move to line
    noremap <Leader>l <Plug>(easymotion-overwin-line)
    noremap <Leader>L <Plug>(easymotion-bd-jk)
     
    " Move to word
    noremap <Leader>w <Plug>(easymotion-overwin-w)
    noremap <Leader>W <Plug>(easymotion-bd-w)
    

endfunction " }}}

function! s:PluginInitFunction_incsearch_vim() " {{{

endfunction " }}}

function! s:PluginInitFunction_incsearch_fuzzy_vim() " {{{

    function! s:config_fuzzyall(...) abort
      return extend(copy({
      \   'converters': [
      \     incsearch#config#fuzzy#converter(),
      \     incsearch#config#fuzzyspell#converter()
      \   ],
      \ }), get(a:, 1, {}))
    endfunction

    noremap <silent><expr> z/ incsearch#go(<SID>config_fuzzyall())
    noremap <silent><expr> z? incsearch#go(<SID>config_fuzzyall({'command': '?'}))
    noremap <silent><expr> zg? incsearch#go(<SID>config_fuzzyall({'is_stay': 1}))


endfunction " }}}

function! s:PluginInitFunction_eskk_vim() " {{{
    let g:eskk#directory = "~/.eskk"
    let g:eskk#dictionary = { 'path': "~/.eskk/.skk-jisyo", 'sorted': 0, 'encoding': 'utf-8', }
    let g:eskk#large_dictionary = { 'path': "~/.eskk/SKK-JISYO.L", 'sorted': 1, 'encoding': 'euc-jp', } 

    let g:eskk#server = {
        \'host': 'localhost',
        \'port': 55100,
    \}

endfunction " }}}

function! s:PluginInitFunction_vim_clang() " {{{
    " let g:clang_cpp_options = '-Wall -Wno-pragma-once-outside-header'
 
    let g:clang_cpp_options = '-Wall -std=c++11 -Wundefined-inline -Wno-pragma-once-outside-header'
    let g:clang_auto = 0
    let g:clang_c_completeopt = 'noinsert,noselect,menuone'
    let g:clang_cpp_completeopt = 'noinsert,noselect,menuone'

    let g:clang_stdafx_h = 'Precompile.hpp'
    let g:clang_compilation_database = './build'


endfunction " }}}

function! s:PluginInitFunction_vim_over() " {{{

endfunction " }}}

function! s:PluginInitFunction_vaffle_vim() " {{{
    let g:vaffle_force_delete = 1
    let g:vaffle_show_hidden_files = 1
    let g:vaffle_use_default_mappings = 1

    "設定そのまま
    function! s:customize_vaffle_mappings() abort
        nmap <buffer> K <Plug>(vaffle-mkdir)
        nmap <buffer> N <Plug>(vaffle-new-file)
    endfunction

    augroup vimrc_vaffle
        autocmd!
        autocmd FileType vaffle call s:customize_vaffle_mappings()
    augroup END


    function! VaffleRenderCustomIcon(item)
        return WebDevIconsGetFileTypeSymbol(a:item.basename, a:item.is_dir)
    endfunction

    let g:vaffle_render_custom_icon = 'VaffleRenderCustomIcon'

 
endfunction " }}}

function! s:PluginInitFunction_fern_vim() " {{{

    "noremap <silent> <C-q> :Fern . -drawer<CR>
  
    " let g:fern#disable_default_mappings = 1

    let g:fern#default_hidden = 1
    " let g:fern#opener = 'edit/vsplit'
    
    if s:is_installed('fern-renderer-nerdfont.vim')
        let g:fern#renderer = "nerdfont"
    "     let g:fern#renderer = "nerdfont"
    "     " let g:fern#renderer = "devicons"
    endif

    function! s:fern_settings() abort
        nmap <silent> <buffer> <expr> <Plug>(fern-quit-or-close-preview) fern_preview#smart_preview("\<Plug>(fern-action-preview:close)", ":q\<CR>")
        nmap <silent> <buffer> q <Plug>(fern-quit-or-close-preview)
    endfunction

    augroup fern-settings
      autocmd!
      autocmd FileType fern call s:fern_settings()
    augroup END    " command! e  <cmd>Fern . -drawer<cr>
 
endfunction " }}}

function! s:PluginInitFunction_quickr_preview_vim() " {{{

    let g:quickr_preview_keymaps = 0
    noremap <leader>; <plug>(quickr_preview)

 
endfunction " }}}

function! s:PluginInitFunction_vim_fugitive() " {{{

    " function! s:update_git_command() abort
    "     let s:gitcmd = 'git'
    "     if 0 == stridx(expand('%:p'), '/mnt/')
    "         let s:gitcmd = 'git.exe'
    "     endif
    " 
    "     echomsg "change git command ".s:gitcmd
    "     let g:fugitive_git_executable = s:gitcmd
    " endfunction
    " 
    " 
    " 
    " if has('unix')
    "     if system('uname -r') =~ "Microsoft"
    "         augroup update_gitcmd 
    "             autocmd!
    "             autocmd BufEnter * call s:update_git_command()
    "         augroup END
    "     endif
    " endif
 
endfunction " }}}

function! s:PluginInitFunction_vim_localvimrc() " {{{


    let g:localvimrc_ask = 0
 

endfunction " }}}


function! s:PluginInitFunction_clever_f_vim() " {{{
    let g:clever_f_across_no_line = 1
    let g:clever_f_ignore_case = 1
    let g:clever_f_smart_case = 1

endfunction " }}}

function! s:PluginInitFunction_fuzzy_motion_vim() " {{{

    " autocmd VimEnter,Colorscheme * :hi FuzzyMotionShade  guifg=red   ctermfg=3
    " autocmd VimEnter,Colorscheme * :hi FuzzyMotionChar  guifg=green   ctermfg=2
    " autocmd VimEnter,Colorscheme * :hi FuzzyMotionSubChar  guifg=blue ctermfg=1
    
    nmap [f :FuzzyMotion <CR>
    
    
endfunction " }}}
function! s:PluginInitFunction_jumpcursor_vim() " {{{

    " nmap [j <Plug>(jumpcursor-jump)
    
endfunction " }}}

function! s:PluginInitFunction_vim_devicons() " {{{
    "let g:webdevicons_enable_unite = 1
    " let g:webdevicons_enable_ctrlp = 1
    let g:WebDevIconsOS = 'Darwin'
    let g:webdevicons_enable = 1
    let g:webdevicons_enable_ctrlp=1
endfunction " }}}

function! s:PluginInitFunction_twitvim() " {{{
    let twitvim_browser_cmd = 'open'
    let twitvim_force_sll = 1
    let twitvim_count = 40
    let twitvim_enable_python3 = 1
endfunction " }}}

function! s:PluginInitFunction_vim_go() " {{{
endfunction " }}}


function! s:PluginInitFunction_echodoc() " {{{
	let g:echodoc_enable_at_startup = 1
endfunction " }}}



function! s:PluginInitFunction_vim_auto_save() " {{{
    let g:auto_save = 1
    let g:auto_save_in_insert_mode = 0
    let g:auto_save_no_updatetime = 800
endfunction " }}}


function! s:PluginInitFunction_hlsl_vim() " {{{
    augroup add-hlsl-setting
        autocmd!
          au! BufRead,BufNewFile *.ush,*.usf setfiletype hlsl 
    augroup END
endfunction " }}}


function! s:is_installed(name)
  return has_key(g:plugs, a:name) ? isdirectory(g:plugs[a:name].dir) : 0
endfunction

if s:is_installed('unite.vim')
    call s:PluginInitFunction_unite_vim()
endif
if s:is_installed('vimfiler')
    call s:PluginInitFunction_vimfiler()
endif
if s:is_installed('Align')
    call s:PluginInitFunction_Align()
endif
if s:is_installed('echodoc')
    call s:PluginInitFunction_echodoc()
endif
if s:is_installed('vim-visualstar')
    call s:PluginInitFunction_vim_visualstar()
endif
if s:is_installed('Indent-Guides')
    call s:PluginInitFunction_Indent_Guides()
endif
if s:is_installed('vim-indent-guides')
    call s:PluginInitFunction_vim_indent_guides()
endif
if s:is_installed('lightline.vim')
    call s:PluginInitFunction_lightline_vim()
endif
if s:is_installed('ctrlp.vim')
    call s:PluginInitFunction_ctrlp_vim()
endif
if s:is_installed('ShowMarks')
    call s:PluginInitFunction_ShowMarks()
endif
if s:is_installed('vim-textmanip')
    call s:PluginInitFunction_vim_textmanip()
endif
if s:is_installed('gundo')
    call s:PluginInitFunction_gundo_vim()
endif
if s:is_installed('vim-anzu')
    call s:PluginInitFunction_vim_anzu()
endif
if s:is_installed('errormarker.vim')
    call s:PluginInitFunction_errormarker_vim()
endif

if s:is_installed('vim-hier')
    call s:PluginInitFunction_vim_hier()
endif

if s:is_installed('vim-typescript')
    call s:PluginInitFunction_typescript_vim()
endif
if s:is_installed('vim-zenspace')
    call s:PluginInitFunction_vim_zenspace()
endif
if s:is_installed('open-browser.vim')
    call s:PluginInitFunction_open_browser_vim()
endif
if s:is_installed('vim-altr')
    call s:PluginInitFunction_vim_altr()
endif
if s:is_installed('vim-easymotion')
    call s:PluginInitFunction_vim_easymotion()
endif

if s:is_installed('incsearch.vim')
    call s:PluginInitFunction_incsearch_vim()
endif

if s:is_installed('incsearch-fuzzy.vim')
    call s:PluginInitFunction_incsearch_fuzzy_vim()
endif

if s:is_installed('caw.vim')
    call s:PluginInitFunction_caw_vim()
endif
if s:is_installed('vim-over')
    call s:PluginInitFunction_vim_over()
endif
if s:is_installed('calendar-vim')
    call s:PluginInitFunction_calendar_vim()
endif
if s:is_installed('vim-quickrun')
    call s:PluginInitFunction_vim_quickrun()
endif
if s:is_installed('vim-watchdogs')
    call s:PluginInitFunction_vim_watchdogs()
endif
if s:is_installed('neocomplete.vim')
    call s:PluginInitFunction_neocomplete_vim()
endif
if s:is_installed('deoplete.nvim')
    call s:PluginInitFunction_deoplete_nvim()
endif
if s:is_installed('deoplete-clang')
    call s:PluginInitFunction_deoplete_clang()
endif
if s:is_installed('project.tar.gz')
    call s:PluginInitFunction_project_tar_gz()
endif
if s:is_installed('vim-showmarks')
    call s:PluginInitFunction_vim_showmarks()
endif
if s:is_installed('omnisharp-vim')
    call s:PluginInitFunction_omnisharp_vim()
endif
if s:is_installed('ale')
    call s:PluginInitFunction_ale()
endif
if s:is_installed('vim-clang')
    call s:PluginInitFunction_vim_clang()
endif
if s:is_installed('vaffle.vim')
    call s:PluginInitFunction_vaffle_vim()
endif
if s:is_installed('eskk.vim')
    call s:PluginInitFunction_eskk_vim()
endif
if s:is_installed('vim-devicons')
    call s:PluginInitFunction_vim_devicons()
endif
if s:is_installed('twitvim')
    call s:PluginInitFunction_twitvim()
endif
if s:is_installed('vim-go')
    call s:PluginInitFunction_vim_go()
endif

if s:is_installed('echodoc')
    call s:PluginInitFunction_echodoc()
endif
if s:is_installed('ultisnips')
    call s:PluginInitFunction_ultisnips()
endif
if s:is_installed('asyncomplete.vim')
    call s:PluginInitFunction_asyncomplete_vim()
endif

if s:is_installed('asyncomplete-ultisnips.vim')
    call s:PluginInitFunction_asyncomplete_ultisnips_vim()
endif

if s:is_installed('asyncomplete-file.vim')
    call s:PluginInitFunction_asyncomplete_file()
endif
if s:is_installed('asyncomplete-emoji.vim')
    call s:PluginInitFunction_asyncomplete_emoji()
endif

if s:is_installed('vim-lsp')
    call s:PluginInitFunction_vim_lsp()
endif


if s:is_installed('vim-lsp-settings')
    call s:PluginInitFunction_vim_lsp_settings()
endif

if s:is_installed('vim-auto-save')
    call s:PluginInitFunction_vim_auto_save()
endif


if s:is_installed('supertab')
    call s:PluginInitFunction_supertab()
endif

if s:is_installed('fzf.vim')
    call s:PluginInitFunction_fzf_vim()
endif

if s:is_installed('vimtweak')
    call s:PluginInitFunction_vimtweak_vim()
endif

if s:is_installed('hlsl.vim')
    call s:PluginInitFunction_hlsl_vim()
endif

if s:is_installed('asyncrun.vim')
    call s:PluginInitFunction_asyncrun_vim()
endif

if s:is_installed('vim-qf-preview')
    call s:PluginInitFunction_vim_qf_preview()
endif

if s:is_installed('editorconfig-vim')
    call s:PluginInitFunction_editorconfig_vim()
endif


if s:is_installed('vim-surround')
    call s:PluginInitFunction_vim_surround()
endif

if s:is_installed('vim-sandwich')
    call s:PluginInitFunction_vim_sandwich()
endif

if s:is_installed('vim-operator-replace')
    call s:PluginInitFunction_vim_operator_replace()
endif

if s:is_installed('fern.vim')
    call s:PluginInitFunction_fern_vim()
endif

if s:is_installed('quickr-preview.vim')
    call s:PluginInitFunction_quickr_preview_vim()
endif
if s:is_installed('vim-fugitive')
    call s:PluginInitFunction_vim_fugitive()
endif

if s:is_installed('vim-localvimrc')
    call s:PluginInitFunction_vim_localvimrc()
endif

if s:is_installed('clever-f.vim')
    call s:PluginInitFunction_clever_f_vim()
endif
if s:is_installed('fuzzy-motion.vim')
    call s:PluginInitFunction_fuzzy_motion_vim()
endif
if s:is_installed('jumpcursor.vim')
    call s:PluginInitFunction_jumpcursor_vim()
endif

if s:is_installed('memolist.vim')
    call s:PluginInitFunction_memolist_vim()
endif

if s:is_installed('vim-clap')
    call s:PluginInitFunction_vim_clap()
endif

if s:is_installed('vim-ripgrep')
    call s:PluginInitFunction_vim_ripgrep()
endif

let g:PluginInitFunctions["colors"] = function('s:PluginInitFunction_colors')


" vim: foldmethod=marker
"
