﻿
if &compatible
  set nocompatible
endif

let s:vim_plugged_dir = expand('$HOME/.cache/vim-plug/plugged/')
let g:vim_plug_repo_dir = s:vim_plugged_dir.'vim-plug'

if &runtimepath !~# '/vim-plug'
    if !isdirectory(g:vim_plug_repo_dir)
        echo 'install vim-plug...'
            if has('win32') || has('win64') || has('win32unix')
                execute '!mkdir ' g:vim_plug_repo_dir
            else
                execute '!mkdir -p' g:vim_plug_repo_dir
            endif
        execute '!git clone https://github.com/junegunn/vim-plug.git' g:vim_plug_repo_dir.'/autoload'
    end

    let s:repo_full_path = fnamemodify(g:vim_plug_repo_dir, ':p')
    set runtimepath^=$HOME/.cache/vim-plug/plugged/vim-plug
endif

call plug#begin(s:vim_plugged_dir)
    Plug 'junegunn/vim-plug', {'dir': g:vim_plug_repo_dir.'/autoload'}
    
        Plug 'prabirshrestha/async.vim'
        Plug 'prabirshrestha/asyncomplete.vim'
        Plug 'prabirshrestha/asyncomplete-file.vim'
        Plug 'prabirshrestha/asyncomplete-lsp.vim'
        Plug 'prabirshrestha/vim-lsp'
        Plug 'mattn/vim-lsp-settings'
        Plug 'hrsh7th/vim-vsnip'
        Plug 'hrsh7th/vim-vsnip-integ'
    
        Plug 'SirVer/ultisnips'
            Plug 'thomasfaingnaert/vim-lsp-snippets'
            Plug 'thomasfaingnaert/vim-lsp-ultisnips'
            Plug 'prabirshrestha/asyncomplete-ultisnips.vim'

        Plug 'easymotion/vim-easymotion'
        Plug 'haya14busa/incsearch.vim'
        Plug 'haya14busa/incsearch-easymotion.vim'
        Plug 'haya14busa/incsearch-fuzzy.vim'

        Plug 'thinca/vim-visualstar'
        Plug 'thinca/vim-zenspace'
        Plug 'thinca/vim-qfreplace'
        Plug 'thinca/vim-localrc'
        Plug 'vim-jp/vimdoc-ja'
        Plug 'mattn/webapi-vim'
        Plug 'nathanaelkane/vim-indent-guides'
    


        Plug 'kana/vim-textobj-user'
          Plug 'osyo-manga/vim-textobj-blockwise'
          Plug 'kana/vim-textobj-function'
          Plug 'thinca/vim-textobj-between'

        Plug 'kana/vim-operator-user'
          Plug 'kana/vim-operator-replace'
    
        Plug 'tyru/open-browser.vim'
        Plug 'tyru/eskk.vim'
        Plug 'taku25/ShowMarks'
        Plug 't9md/vim-textmanip'
        Plug 'sjl/gundo.vim'

        Plug 'machakann/vim-sandwich'

        Plug 'vim-scripts/visualctrlg'
        Plug 'osyo-manga/vim-anzu'

        Plug 'rbtnn/vim-ambiwidth'

        Plug 'osyo-manga/vim-over'
        Plug 'vim-scripts/errormarker.vim'
        Plug 'dannyob/quickfixstatus'
        Plug 'taku25/vim-hier'
        Plug 'kana/vim-altr'
        Plug 'tyru/caw.vim'
        " Plug 'cocopon/vaffle.vim'
        Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
        " Plug 'junegunn/fzf.vim'

        Plug 'lambdalisue/gin.vim'
        Plug 'lambdalisue/fern.vim', { 'branch': 'main' } 
            Plug 'lambdalisue/fern-hijack.vim'
            Plug 'lambdalisue/nerdfont.vim'
            Plug 'lambdalisue/fern-renderer-nerdfont.vim'
            Plug 'yuki-yano/fern-preview.vim'
            " Plug 'LumaKernel/fern-mapping-fzf.vim'

        Plug 'tpope/vim-fugitive'
        Plug 'idanarye/vim-merginal'
        Plug 'tyru/eskk.vim'
   


        Plug 'cocopon/colorswatch.vim'
        Plug 'ryanoasis/vim-devicons'
        Plug 'leafgarland/typescript-vim'
        Plug 'honza/vim-snippets'
        Plug 'Shougo/echodoc'
        Plug 'skanehira/translate.vim'
        Plug 'tpope/vim-abolish'
        Plug 'vim-scripts/vim-auto-save'
        Plug 'vim/killersheep'


        Plug 'thinca/vim-quickrun'
            Plug 'osyo-manga/shabadou.vim'
    
        Plug 'vim-scripts/Align'
    
        Plug 'plasticboy/vim-markdown'
    

        Plug 'mattn/vim-goimports'


        Plug 'ervandew/supertab'

        Plug 'markonm/traces.vim'

        Plug 'cocopon/pgmnt.vim'

        if has('mac')
            Plug 'itchyny/dictionary.vim'
        endif


        Plug 'junegunn/vim-easy-align'

        " Plug 'editorconfig/editorconfig-vim'
        Plug 'beyondmarc/hlsl.vim'
 
	    Plug 'skywind3000/asyncrun.vim'
        
        Plug 'simeji/winresizer'
        
   
        Plug 'itchyny/vim-cursorword'
        
        Plug 'itchyny/lightline.vim'
       	    Plug 'halkn/lightline-lsp'
            Plug 'micchy326/lightline-lsp-progress'
            Plug 'cocopon/lightline-hybrid.vim'
       	    Plug 'yami-beta/lightline-pencil.vim'


        Plug 'reedes/vim-colors-pencil'
        Plug 'w0ng/vim-hybrid'
        Plug 'cocopon/iceberg.vim'
        Plug 'NLKNguyen/papercolor-theme'

        Plug 'EdenEast/nightfox.nvim'


        Plug 'vim-denops/denops.vim'
            Plug 'yuki-yano/fuzzy-motion.vim'

        
        " Plug 'skanehira/jumpcursor.vim'

        
        Plug 'rhysd/clever-f.vim'

        Plug 'andymass/vim-matchup'
        Plug 'github/copilot.vim'

        Plug 'bfrg/vim-cpp-modern'

        Plug 'lifepillar/vim-colortemplate'

        " Plug 'liuchengxu/vim-clap', { 'tag': 'v0.45', 'do': ':Clap install-binary' }
        Plug 'liuchengxu/vim-clap', {  'do': ':Clap install-binary' }
        "
        Plug 'glidenote/memolist.vim'

        Plug 'cohama/agit.vim'

        Plug 'kyoh86/vim-ripgrep'


call plug#end()

function! s:check_installation()
  if empty(g:plugs)
    return 0
  endif

  let list = []
  for [name, spec] in items(g:plugs)
    if !isdirectory(spec.dir)
      call add(list, spec.uri)
    endif
  endfor

  return len(list) > 0 ? 1 : 0
endfunction

if has('vim_starting') && s:check_installation()
    PlugInstall 
endif

