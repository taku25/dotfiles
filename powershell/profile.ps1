oh-my-posh init pwsh --config ~/prompt.json | Invoke-Expression

# $Env:PYTHONUTF8=1 

$Env:Path+=';'+$HOME+'/AppData/Local/Programs/fzf'
$Env:Path+=';'+$HOME+'/AppData/Local/Programs/ripgrep'
$Env:Path+=';'+$HOME+'/AppData/Local/Programs/deno-x86_64-pc-windows-msvc'
$Env:Path+=';'+$HOME+'/AppData/Local/Programs/Python/Python311'
$Env:Path+=';'+$HOME+'/AppData/Local/Programs/Python/Python311/Scripts'
$Env:Path+=';'+$HOME+'/.cargo/bin'
$Env:Path+=';'+$HOME+'/AppData/Local/Programs/git-submodule-ls-files'

$Env:Path+=';C:/Program Files/Vim/vim91'

#$Env:Path+=';C:/Program Files/nodejs'
#$Env:Path+=';'+$HOME+'/AppData/Local/Programs/vim90'

# $Env:Path+=';C:/Program Files/Git/usr/bin'

# $ENV:FZF_DEFAULT_COMMAND='rg --files --hidden --follow --glob "!.git/*"'
$ENV:FZF_DEFAULT_OPTS=" --no-sort --exact --cycle --multi --ansi --reverse --border --bind ctrl-a:select-all"
# $ENV:FZF_DEFAULT_OPTS+=" --color=bg+:-1,bg:-1,spinner:#84a0c6,hl:#6b7089,fg:#c6c8d1,header:#6b7089"
$ENV:FZF_DEFAULT_OPTS+=" --color=info:#b4be82,pointer:#84a0c6,marker:#84a0c6,fg+:#c6c8d1,prompt:#84a0c6,hl+:#84a0c6"
$ENV:FZF_DEFAULT_OPTS+=" --color=bg+:#1e2132,bg:#161821,spinner:#84a0c6,hl:#6b7089,fg:#c6c8d1,header:#6b7089"

$Env:RIPGREP_CONFIG_PATH=$HOME+'/.ripgreprc'

Import-Module PSReadLine
Import-Module PSFzf
Import-Module ZLocation
Import-Module Terminal-Icons 

Enable-PsFzfAliases
# ZLocationの読み込み


Remove-Item alias:ls
function ls() { Get-ChildItem -force }

Set-PsFzfOption -PSReadlineChordProvider 'Ctrl+f' -PSReadlineChordReverseHistory 'Ctrl-r'

Set-PSReadLineOption -PredictionSource History -PredictionViewStyle ListView

Set-PSReadLineKeyHandler -Key Tab -Function MenuComplete
Set-PSReadLineKeyHandler -Chord 'Ctrl+h' -ScriptBlock  { Invoke-FuzzyZLocation; [Microsoft.PowerShell.PSConsoleReadLine]::AcceptLine() }

Function touch($file) {
  If (Test-Path $file) {
    (Get-Item $file).LastWriteTime = Get-Date
  } Else {
    Out-File -encoding Default $file
  }
}

Function open($x = '.') {
    Invoke-Item $x
}

Function cd-git-root(){
  $rotDir = git rev-parse --show-superproject-working-tree
  Write-Output $rootDir
  if ([String]::IsNullOrEmpty($rootDir)){
    $rootDir = git rev-parse --show-toplevel
    Write-Output $rootDir
  }
  cd $rootDir
}

Set-Alias tig "C:/Program Files/Git/usr/bin/tig.exe"
