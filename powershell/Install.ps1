
#管理者権限で実行されていなければ、スクリプトを管理者権限で実行し直す
If (-not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')){
    Start-Process -FilePath PowerShell.exe -ArgumentList "-NoLogo -ExecutionPolicy Bypass -File $($MyInvocation.MyCommand.Path)" -Verb RunAs
    Exit
}

$path = Split-Path -Parent $MyInvocation.MyCommand.Path
Set-Location $path




#power shell
if (-not (Test-Path $HOME\Documents\PowerShell )){
    New-Item $HOME\Documents\PowerShell -ItemType Directory
}
New-Item -Path $HOME\Documents\PowerShell -Name Microsoft.PowerShell_profile.ps1 -Value $path\profile.ps1 -ItemType SymbolicLink
New-Item -Path $HOME -Name prompt.json -Value $path\prompt.json -ItemType SymbolicLink

#vim
New-Item -Path $HOME -Name _vimrc -Value $path\..\_vimrc -ItemType SymbolicLink
New-Item -Path $HOME -Name .vim -Value $path\..\vimfiles -ItemType SymbolicLink

$host.UI.RawUI.ReadKey()