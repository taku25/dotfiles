#!/bin/zsh

CURRENT_DIR=$(cd $(dirname $0); pwd)

ln -s $CURRENT_DIR/vimfiles ~/.vim
ln -s $CURRENT_DIR/zsh/_zplugrc ~/.zplugrc

ln -s $CURRENT_DIR/alacritty/alacritty.yml ~/.config/alacritty/alacritty.yml

ln -s $CURRENT_DIR/zsh/_zshenv ~/.zshenv
ln -s $CURRENT_DIR/zsh/_zshrc ~/.zshrc
ln -s $CURRENT_DIR/zsh/_zshrc ~/.ztesttest

ln -s $CURRENT_DIR/tmux/_tmux.conf  ~/.tmux.conf
ln -s $CURRENT_DIR/tmux/_tmux.osx.conf  ~/.tmux.osx.conf
ln -s $CURRENT_DIR/ripgrep/_ripgreprc  ~/.ripgreprc

ln -s $CURRENT_DIR/_vimrc  ~/.vimrc
ln -s $CURRENT_DIR/wrt  /usr/local/bin/wrt

mkdir -p ~/.cache/vim/backup
mkdir -p ~/.cache/shell

if [ ! -e "${HOME}/.anyenv" ]; then
    printf "anyenv Install? [y/N]: "
    if read -q; then
        git clone https://github.com/anyenv/anyenv ~/.anyenv
    fi
fi

if [[ "$OSTYPE" == "linux-gnu" ]]; then
    if [  -n "$(uname -a | grep Ubuntu)" ]; then
        printf "Install lib? [y/N]: "
        if read -q; then
            sudo apt update
            sudo apt upgrade

            sudo apt install build-essential libbz2-dev libdb-dev cmake
            sudo apt install gettext libtinfo-dev libncurses5-dev clang autoconf automake cproto
            sudo apt install libreadline-dev libffi-dev libgdbm-dev liblzma-dev
            sudo apt install libsqlite3-dev libssl-dev
            sudo apt install zlib1g-dev uuid-dev tk-dev
            sudo apt install perl-dev
        fi
    fi  
fi
