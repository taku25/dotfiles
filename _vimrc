﻿set encoding=utf-8
set termencoding=utf-8
scriptencoding utf-8
set fileencodings=ucs-bom,utf-8,iso-2022-jp,euc-jp,cp932,latin1
"標準はunix基準 変更したい場合は local...で変更
set fileformats=unix,mac,dos
set nocompatible
"すぺーすにしてみる
let mapleader = "\<Space>"
"ftpluginを個別に無効化するのがめんどくさいので
"最初に読んで後から上書き
"dein後にonにするとhook系で設定したautocmd filetype系がしぬ
"let g:DISABLE_USER_PLUGIN_VIM = 1

" if has('nvim')
"     let g:python3_host_prog = $PYENV_ROOT . '/shims/python3'
" endif
if has('python3')
    silent! python3 1
endif

filetype plugin indent on

if !has('gui_running')
    set termguicolors

endif

let $VIMFILE_DIR = '.vim'
if has('win32') || has('win64') || has('win32unix')
    let $LOCAL_OS_TYPE_NAME = 'win'
else
    if has('mac')
        let $LOCAL_OS_TYPE_NAME = 'mac'
    else
        let $LOCAL_OS_TYPE_NAME = 'linux'
    endif
endif

let g:LOCAL_VIMSETTING_PLUGIN_SETTING = 'PLUGIN'
let g:LOCAL_VIMSETTING_BUNDLE_SOURCE  = 'BUNDLE' 
let g:LOCAL_VIMSETTING_BEGIN_VIMRC    = 'BEGIN_VIM'
let g:LOCAL_VIMSETTING_END_VIMRC      = 'END_VIM'
let g:LOCAL_VIMSETTING_BEGIN_GVIMRC   = 'BEGIN_GVIM'
let g:LOCAL_VIMSETTING_END_GVIMRC     = 'END_GVIM'

let g:DISABLE_LOCAL_VIM = 0
let g:DISABLE_LOCAL_OS_VIM = 0
let g:DISABLE_USER_PLUGIN_VIM = 0
let g:DISABLE_USER_PLUGIN_SETTING_VIM = 0

let g:LOCAL_VIM_FILE_DIR_PATH = expand('$HOME/$VIMFILE_DIR/')
let g:LOCAL_RC_DIR_PATH = expand('$HOME/$VIMFILE_DIR/localrc/')
let g:LOCAL_RC_PATH = g:LOCAL_RC_DIR_PATH.'localrc.vim'
let g:LOCAL_OS_RC_PATH = g:LOCAL_RC_DIR_PATH.'localrc.'.$LOCAL_OS_TYPE_NAME.'.vim'
let g:BUNDLE_RC_PATH = g:LOCAL_VIM_FILE_DIR_PATH.'bundlerc.vim'
let g:PLUGIN_RC_PATH = g:LOCAL_VIM_FILE_DIR_PATH.'pluginrc.vim'


if filereadable(g:LOCAL_RC_PATH) == 0
    let g:DISABLE_LOCAL_VIM = 1
endif

if filereadable(g:LOCAL_OS_RC_PATH) == 0
    let g:DISABLE_LOCAL_OS_VIM = 1
endif

if filereadable(g:PLUGIN_RC_PATH) == 0
    let g:DISABLE_USER_PLUGIN_SETTING_VIM = 1
endif

if filereadable(g:BUNDLE_RC_PATH) == 0
    let g:DISABLE_USER_PLUGIN_VIM = 1
endif


function! s:LocalRCFunction_None()
endfunction

let g:user_local_rc_function_list = {}
let g:user_local_rc_function_list = {
            \ g:LOCAL_VIMSETTING_PLUGIN_SETTING : {'OSShare': function('s:LocalRCFunction_None'), 'Local': function('s:LocalRCFunction_None'), },
            \ g:LOCAL_VIMSETTING_BUNDLE_SOURCE : {'OSShare': function('s:LocalRCFunction_None'), 'Local': function('s:LocalRCFunction_None'), },
            \ g:LOCAL_VIMSETTING_BEGIN_VIMRC : {'OSShare': function('s:LocalRCFunction_None'), 'Local': function('s:LocalRCFunction_None'), },
            \ g:LOCAL_VIMSETTING_END_VIMRC : {'OSShare': function('s:LocalRCFunction_None'), 'Local': function('s:LocalRCFunction_None'), },
            \ g:LOCAL_VIMSETTING_BEGIN_GVIMRC : {'OSShare': function('s:LocalRCFunction_None'), 'Local': function('s:LocalRCFunction_None'), },
            \ g:LOCAL_VIMSETTING_END_GVIMRC : {'OSShare': function('s:LocalRCFunction_None'), 'Local': function('s:LocalRCFunction_None'), },
            \}

"ローカル設定
if g:DISABLE_LOCAL_VIM == 0
    execute 'so '.g:LOCAL_RC_PATH
endif

if g:DISABLE_LOCAL_OS_VIM == 0
    execute 'so '.g:LOCAL_OS_RC_PATH
endif

"このテーブルうまいことかけないかなぁ〜
function! s:PluginInitFunction_None()
endfunction
let g:PluginInitFunctions = {}
let g:PluginInitFunctions["colors"] = function('s:PluginInitFunction_None')


"ソース
if has('python3')
    filetype off
    if g:DISABLE_USER_PLUGIN_VIM == 0
        execute 'so '.g:BUNDLE_RC_PATH
    endif
    filetype plugin indent on

    :set modifiable

    let g:enable_lsp_log = 0
    "let g:my_color_scheme = "pencil"
    let g:transparent = 1
    " let g:my_color_scheme = "brise"
    let g:my_color_scheme = "iceberg_d"
    " let g:my_color_scheme = "iceberg_l"


    "プラグイン設定関数定義
    let s:None_Function = function('s:PluginInitFunction_None')
    if g:DISABLE_USER_PLUGIN_SETTING_VIM == 0
        execute 'so '.g:PLUGIN_RC_PATH
    else
    endif

    augroup vim-colorsheme
        autocmd!
        autocmd VimEnter * call g:PluginInitFunctions["colors"]()
    augroup END
endif

call g:user_local_rc_function_list[g:LOCAL_VIMSETTING_BUNDLE_SOURCE]['OSShare']()
call g:user_local_rc_function_list[g:LOCAL_VIMSETTING_BUNDLE_SOURCE]['Local']()

call g:user_local_rc_function_list[g:LOCAL_VIMSETTING_PLUGIN_SETTING]['OSShare']()
call g:user_local_rc_function_list[g:LOCAL_VIMSETTING_PLUGIN_SETTING]['Local']()


call g:user_local_rc_function_list[g:LOCAL_VIMSETTING_BEGIN_VIMRC]['OSShare']()
call g:user_local_rc_function_list[g:LOCAL_VIMSETTING_BEGIN_VIMRC]['Local']()

if has('kaoriya')
    let g:vimrc_local_finish = 1
    let g:vimrc_first_finish = 1
    let g:no_gvimrc_example = 1
endif

let s:is_winos = 0
if executable('uname')
    if system('uname -r') =~ "Microsoft"
        let s:is_winos = 1
    endif
else
    if has('win32') || has('win64') || has('win32unix')
        let s:is_winos = 1
    endif
endif

" if s:is_winos == 1 && exists('+shellslash')
"  set noshellslash
" endif


if !has('gui_running')
    if has('mouse')
        set mouse=a
        if !has('nvim')
            set ttymouse=xterm2
        endif
    endif
    set ttyfast
    set lazyredraw


    if !has('nvim')
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
        let &t_8u = "\<Esc>[58;2;%lu;%lu;%lum"
    endif
    




   "cui時にインサートモードの時とノーマルモードでマウスカーソルを変更する
   if s:is_winos == 1
   
        let &t_SI .= "\e[6 q"
        let &t_EI .= "\e[2 q"
        let &t_SR .= "\e[3 q"

   elseif &term =~ "xterm"

       if $TMUX != '' && executable('tmux') == 1
           let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
           let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
           let &t_SR = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShame=2\x7\<Esc>\\"
       else
           let &t_SI = "\e]50;CursorShape=1\x7"
           let &t_EI = "\e]50;CursorShape=0\x7"
           let &t_SR = "\e]50;CursorShape=2\x7"
       endif

   elseif &term =~ "screen"
       let &t_SI = "\ePtmux;\e\e]50;CursorShape=1\x7\e\\"
       let &t_EI = "\ePtmux;\e\e]50;CursorShape=0\x7\e\\"
   endif
    " let &t_Cs = "\e[4:3m"
    " let &t_Ce = "\e[4:0m"

   " if (&t_Co != 256)
   "     set t_Co=256
   " endif

    " Use vsplit mode
    " if has("vim_starting") && has('vertsplit')
    "   function! EnableVsplitMode()
    "     " enable origin mode and left/right margins
    "     let &t_CS = "y"
    "     let &t_ti = &t_ti . "\e[?6;69h"
    "     let &t_te = "\e[?6;69l\e[999H" . &t_te
    "     let &t_CV = "\e[%i%p1%d;%p2%ds"
    "     call writefile([ "\e[?6;69h" ], "/dev/tty", "a")
    "   endfunction
    " 
    "   " old vim does not ignore CPR
    "   map <special> <Esc>[3;9R <Nop>
    " 
    "   " new vim can't handle CPR with direct mapping
    "   " map <expr> ^[[3;3R EnableVsplitMode()
    "   set t_F9=[3;3R
    "   " map <expr> <t_F9> EnableVsplitMode()
    "   let &t_RV .= "\e[?6;69h\e[1;3s\e[3;9H\e[6n\e[0;0s\e[?6;69l"
    " 
    " endif


    if executable('uname')
        if system('uname -r') =~ "Microsoft"
          augroup WSLYank
            autocmd!
            autocmd TextYankPost * :call system('gocopy.exe',@")
          augroup END



          function! WSLPasteFunc()
            " :execute "r! pwsh.exe -Command Get-Clipboard"
            :execute "r! gopaste.exe"
          endfunction 
          command! WSLPaste call WSLPasteFunc()
          nmap <Leader>wp :WSLPaste<CR>
      endif
    endif
endif

" if has('nvim')
    " :tnoremap <Esc> <C-\><C-n>
    " set sh=zsh
    " let $NVIM_TUI_ENABLE_TRUE_COLOR=1
" endif
  " set shell=pwsh
if has('nvim') && s:is_winos == 1
  " set shell=pwsh
  " set shellcmdflag=-NoLogo\ -Command
  " set shellquote=\"
  " set shellxquote=


" 'shellcmdflag'	-c
" 'shellxquote'	"
" 'shellpipe'	>%s 2>&1
" 'shellredir'	>%s 2>&1
endif

set belloff=all




"行番号表示
set number relativenumber

"サインは常に表示
set signcolumn=yes

"画面の端に行っても折り返さない
set nowrap

"オートインデント
set autoindent

set autoread

"幅
" set ambiwidth=double
set ambiwidth=single

set emoji


"ヒストリーは300位保持
set history=300

"検索時に大文字小文字を無視する
set ignorecase

"検索に大文字が入っていたら大文字小文字分けて検索
set smartcase

"下まで検索したらもどる
set wrapscan

"検索中にハイライト
set incsearch

set showmatch

"検索ハイライト
set hlsearch

"拡張保管
set wildmenu

"自動改行しない
set textwidth=0

"タブはスペース4つ
set tabstop=4
"タブはスペース4つ
set softtabstop=4
"インデントも4
set shiftwidth=4
"タブをスペースに
set expandtab

"起動メッセージ表示しない
set shortmess+=Is

"undoを隠しバッファーでも表示
set hidden

"クリップボード同期
"set clipboard+=unnamedplus
if has('unnamedplus')
    set clipboard+=unnamedplus
else
    set clipboard+=unnamed

endif
"ステータスラインにコマンド表示
set showcmd


"スワップファイルを作らない
set noswapfile



function! s:MakeDirectory(path)
    if !isdirectory(a:path)
        if has('win32') || has('win64') || has('win32unix')
            execute '!mkdir ' a:path
        else
            execute '!mkdir -p' a:path
        endif
    end
endfunction

"バックアップファイルをつくる
call s:MakeDirectory(expand('$HOME/.cache/vim/backup/'))
set backupdir=$HOME/.cache/vim/backup
set backup

"undoファイルを作る
call s:MakeDirectory(expand('$HOME/.cache/vim/undo/'))
set undodir=$HOME/.cache/vim/undo
set undofile

set autoread
" set autowrite
" autocmd CursorHold *  wall
" autocmd CursorHoldI *  wall

" コピペ
"nnoremap d; diw"0P

"見た目で移動(おりかえししないけど一応
nnoremap j gj
nnoremap k gk

"削除した時にレジスタに格納なんかしない
"nnoremap x "_x
"nnoremap dd "_dd


"ステータスラインにいろいろ表示
"default to lightline.vim
"set statusline=%n\:%y%F\ \|%{(&fenc!=''?&fenc:&enc).(&bomb?':BOM':'').'\|'.&ff.'\|'}%m%r%=<%l/%L%h\ %<%=%{strftime(\"%I:%M:%S\")} 


"map <F1> <ESC>:bp<CR>
"map <F2> <ESC>:bn<CR>

"map <F3> <ESC>:tabp<CR>
"map <F4> <ESC>:tabn<CR>
"


nnoremap <silent> cy  ce<C-r>0<ESC>:let@/=@1<CR>:noh<CR>
vnoremap <silent> cy  c<C-r>0<ESC>:let@/=@1<CR>:noh<CR>
vnoremap <silent> ciy ciw<C-r>0<ESC>:let@/=@1<CR>:noh<CR>

nnoremap <ESC><ESC> :nohlsearch<CR>

"自動整形しない
set formatexpr=""

"常にタブline表示
set showtabline=2

"ステータスライン常に表示
set laststatus=2
"コマンドライン
set cmdheight=2

" バックスペースでインデントや改行を削除できるようにする
set backspace=indent,eol,start

"コマンドライン補間強化
set wildmenu

"ルーラー表示
set ruler

set list
set listchars=tab:>-,trail:⇨,eol:↲

"タイトル表示
set title

"３行みせて
set scrolloff=3

"一つでも表示

if !has('nvim')
    set completeopt=menuone,popup
endif
" autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
" autocmd InsertLeave * if pumvisible() == 0|pclose|endif
" autocmd CompleteDone * silent! pclose!

" カラー設定:
"syntax enable
"デフォルト(pluginがインストールされてる場合はpluginsettingで変更される
"colorscheme desert

" カレント行ハイライトON
set cursorline

" set synmaxcol=300


command! -nargs=0 CdCurrent cd %:p:h

function! s:YankCommandOutputFunc(command)
    redir @*
    silent execute a:command
    redir END
endfunction

command! -nargs=1 -complete=command YankCommandOutput call s:YankCommandOutputFunc(<q-args>)


"command! -nargs=0 -complete=command ClearQuickfixList call s:ClearQuickfixList()

"vim簡単終了
command! Quit bufdo q! 

map q: <nop>
nnoremap Q <nop>
command! ClearQuickfixList cexpr []


set diffopt+=vertical

augroup my-filetype 
    autocmd!
    autocmd BufRead,BufNewFile,BufReadPre *.coffee   set filetype=coffee
augroup END



call g:user_local_rc_function_list[g:LOCAL_VIMSETTING_END_VIMRC]['OSShare']()
call g:user_local_rc_function_list[g:LOCAL_VIMSETTING_END_VIMRC]['Local']()

noremap :: q:i
noremap // q/i
command W w
command Q q

augroup my-commnad-window
    autocmd!
    autocmd CmdwinEnter * nnoremap <buffer> q :<C-u>quit<CR>  
augroup END

if executable('rg')
    let &grepprg = 'rg --vimgrep --no-heading'
    set grepformat=%f:%l:%c:%m,%f:%l:%m
endif



nnoremap <silent> <leader>e :ed .<CR>
nnoremap <silent> <leader>y :YankCommandOutput echo expand('%:p') <CR>
nnoremap <silent> <leader>p :echo expand('%:p') <CR>
nnoremap <silent> <leader>v :ed ~/.vim <CR>



" if executable('fzf')
" 
"     function! OpenFileFromFzf() abort
"         " echom "start"
"         let l:targetFileName = expand("<cfile>")
"         if has('win32') || has('win64')
"             let l:targetFileName = substitute(l:targetFileName, '/','\', "g")
"         endif
"         let l:result = system("fzf -1 -q ".l:targetFileName)
"         " let l:filename = substitute(l:result, '\n|\r\n|\r',"", "g")
"         
"         execute('e '.l:result)
" 
"     endfunction
" 
"     noremap <silent> <leader>gf :call OpenFileFromFzf()<CR>
" 
" endif


syntax on


