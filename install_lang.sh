#!/bin/zsh


if type anyenv >/dev/null 2>&1; then
    anyenv install pyenv
    anyenv install luaenv
    anyenv install plenv
    anyenv install rbenv
fi


PY2_VERSION=2.7.17
PY3_VERSION=3.9.0
LUAJIT_VERSION=luajit-2.0.5
LUA_VERSION=5.4.0

# PERL_VERSION=5.30.1
RUBY_VERSION=2.7.0

echo "install py2 version "$PY2_VERSION
echo "install py3 version "$PY3_VERSION
echo "install luajit version "$LUAJIT_VERSION
echo "install perl version "$PERL_VERSION
echo "install ruby version "$RUBY_VERSION

PY2_CONFIG=""
PY3_CONFIG=""
LUAJIT_CONFIG=""
LUA_CONFIG=""
# PERL_CONFIG=""
RUBY_CONFIG=""

# PERL_CONFIG+='CONFIGURE_OPTS="--enable-shared"'
RUBY_CONFIG+='RUBY_CONFIGURE_OPTS="--enable-shared"'

case "${OSTYPE}" in
    darwin*)
    PY2_CONFIG+='PYTHON_CONFIGURE_OPTS="--enable-framework" '
    PY2_CONFIG+='CONFIGURE_OPTS="--with-openssl=/usr/local/opt/openssl@1.1" '
    PY2_CONFIG+='CFLAGS="-I/usr/local/opt/readline/include -I/usr/local/opt/openssl@1.1/include" '
    PY2_CONFIG+='LDFLAGS="-L/usr/local/opt/readline/lib -L/usr/local/opt/openssl@1.1/lib" '
    PY2_CONFIG+='SDKROOT="/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk"'

    PY3_CONFIG+='PYTHON_CONFIGURE_OPTS="--enable-framework" '
    #PY3_CONFIG+='CONFIGURE_OPTS="--with-openssl=/usr/local/opt/openssl@1.1" '
    #PY3_CONFIG+='CFLAGS="-I/usr/local/opt/readline/include -I/usr/local/opt/openssl@1.1/include" '
    #PY3_CONFIG+='LDFLAGS="-L/usr/local/opt/readline/lib -L/usr/local/opt/openssl@1.1/lib" '
    PY3_CONFIG+='SDKROOT="/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk"'

    LUAJIT_CONFIG+='MAXOSX_DEPLOYMENT_TARGET=10.14 '
    LUAJIT_CONFIG+='CONFIGURE_OPTS="--enable-shared"'

    ;;
    linux-gnu*)
    PY2_CONFIG+='PYTHON_CONFIGURE_OPTS="--enable-shared"'
    PY3_CONFIG+='PYTHON_CONFIGURE_OPTS="--enable-shared"'
    LUAJIT_CONFIG+='CONFIGURE_OPTS="--enable-shared"'
    LUA_CONFIG+='CONFIGURE_OPTS="--enable-shared"'
    ;; 
esac


eval "$PY2_CONFIG pyenv install $PY2_VERSION"
eval "$PY3_CONFIG pyenv install $PY3_VERSION"
eval "$LUAJIT_CONFIG luaenv install $LUAJIT_VERSION"
eval "$LUA_CONFIG luaenv install $LUA_VERSION"
# eval "$PERL_CONFIG plenv install $PERL_VERSION"
eval "$RUBY_CONFIG rbenv install $RUBY_VERSION"

